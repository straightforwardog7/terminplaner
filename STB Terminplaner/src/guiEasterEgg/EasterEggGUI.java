package guiEasterEgg;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import logic.ControllerEasterEgg;
import utils.Gravitation;
import utils.Kollision;

public class EasterEggGUI extends JFrame implements KeyListener {

	private static final long serialVersionUID = 1L;
	public static GameScreen gameScreen;
	public static Kollision kollision;
	public static Gravitation gravitation;
	static BufferStrategy buffer;
	public static boolean debugOn = false;
	// Input input;

	public void start() {
		kollision.start();
		gravitation.start();
		gameScreen.start();
	}

	/*
	 * Konstruktor der Spiel-GUI
	 */
	public EasterEggGUI() {
		setIconImage(ControllerEasterEgg.tenbusch.getTenbuschBild());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setBounds(250, 100, 1300, 900);
		setResizable(false);
		gameScreen = new GameScreen();
		gameScreen.setSize(new Dimension(1300, 900));
		gameScreen.setIgnoreRepaint(true);
		gameScreen.setVisible(true);
		addKeyListener(this);
		add(gameScreen);
		addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {}
			
			@Override
			public void windowIconified(WindowEvent e) {}
			
			@Override
			public void windowDeiconified(WindowEvent e) {}
			
			@Override
			public void windowDeactivated(WindowEvent e) {}
			
			@Override
			public void windowClosing(WindowEvent e) {}
			
			@Override
			public void windowClosed(WindowEvent e) {
				gameScreen.setBeenden(true);
				gameScreen.setPause(true);
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {}
		});
		kollision = new Kollision();
		gravitation = new Gravitation();
		setVisible(true);
	}

	/*
	 * Erstellt eine BufferStrategy, damit mit Hilfe eines 2-Frame-Buffers
	 * gezeichnet werden kann.
	 */
	public void erstelleBuffer() {
		gameScreen.createBufferStrategy(2);
		buffer = gameScreen.getBufferStrategy();
	}

	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			ControllerEasterEgg.sprung();
			if (gameScreen.isPause()) {
				gameScreen.setPause(false);
			}
		} else if (e.getKeyCode() == KeyEvent.VK_A) {
			if (debugOn)
				debugOn = false;
			else
				debugOn = true;
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {}

}
