package guiEasterEgg;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import logic.ControllerEasterEgg;
import utils.FPS;
//import utils.Input;
import utils.Score;

public class GameScreen extends Canvas implements Runnable {

	// Attribute
	private static final long serialVersionUID = 1L;
	Thread gameScreenThread = new Thread(this);
	private boolean pause = false;
	private boolean beenden = false;
	private int rotationsFaktor;
	private double rotation;

	//Getter und Setter
	public boolean isPause() {
		return pause;
	}

	public void setPause(boolean pause) {
		this.pause = pause;
	}

	public boolean isBeenden() {
		return beenden;
	}

	public void setBeenden(boolean beenden) {
		this.beenden = beenden;
	}

	// Methoden
	
	/*
	 * Zeichnet die aktuellesten Daten
	 */
	public void render(long lastFPSupdate) {

		// Graphics-Objekte zum Zeichen
		Graphics graphics = EasterEggGUI.buffer.getDrawGraphics();
		Graphics graphicsT = EasterEggGUI.buffer.getDrawGraphics();
		Graphics2D graphics2D = (Graphics2D) graphics;
		Graphics2D graphicsTenbusch = (Graphics2D) graphicsT;

		// Kantenglaettung aktivieren
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// Hintergrund
		graphics2D.drawImage(ControllerEasterEgg.hintergrund1.getHintergrundBild(),
				ControllerEasterEgg.hintergrund1.getPosX(), 0, null);
		ControllerEasterEgg.hintergrund1.setPosX(ControllerEasterEgg.hintergrund1.getPosX() - 1);
		graphics2D.drawImage(ControllerEasterEgg.hintergrund2.getHintergrundBild(),
				ControllerEasterEgg.hintergrund2.getPosX(), 0, null);
		ControllerEasterEgg.hintergrund2.setPosX(ControllerEasterEgg.hintergrund2.getPosX() - 1);
		// Tenbusch zeichnen
		if (ControllerEasterEgg.tenbusch.getGeschwindigkeit() < 0)
			rotationsFaktor = -30;
		else
			rotationsFaktor = -50;
		rotation = (double) ControllerEasterEgg.tenbusch.getGeschwindigkeit() / rotationsFaktor;
		if(rotation >= 1)
			rotation = 1;
		graphicsTenbusch.rotate(rotation, ControllerEasterEgg.tenbusch.getCenterPosX(),
				ControllerEasterEgg.tenbusch.getCenterPosY());
		graphicsTenbusch.drawImage(ControllerEasterEgg.tenbusch.getTenbuschBild(),
				ControllerEasterEgg.tenbusch.getPosX(), ControllerEasterEgg.tenbusch.getPosY(), null);
		if (EasterEggGUI.debugOn) {
			// Hitbox zeichnen
			graphics2D.setColor(Color.white);
			graphics2D.drawOval(ControllerEasterEgg.tenbusch.getPosX(), ControllerEasterEgg.tenbusch.getPosY(),
					ControllerEasterEgg.tenbusch.getWidth(), ControllerEasterEgg.tenbusch.getHeight());
			// FPS anzeigen

			graphics2D.setFont(new Font("Courier New", Font.PLAIN, 16));
			graphics2D.drawString(String.format("FPS: %s", FPS.fpsBackup), 20, 20);
			if (lastFPSupdate >= 1000000000)
				FPS.fpsBackup = FPS.fps;
		}
		// pipes zeichnen

		for (int i = 0; i < ControllerEasterEgg.pipes.size(); i++) {
			ControllerEasterEgg.pipes.get(i).setPosX(ControllerEasterEgg.pipes.get(i).getPosX() - 3);
			graphics2D.drawImage(ControllerEasterEgg.pipes.get(i).getPipe(), ControllerEasterEgg.pipes.get(i).getPosX(),
					ControllerEasterEgg.pipes.get(i).getPosY(), null);
		}

		// Score anzeigen;
		graphics2D.setColor(Color.white);
		graphics2D.setFont(new Font("Comic Sans MS", Font.PLAIN, 30));
		graphics2D.drawString(String.format("Score: %s", Score.getScore()), 530, 50);

		// Highscore anzeigen;
		graphics2D.setFont(new Font("Comic Sans MS", Font.PLAIN, 30));
		graphics2D.drawString(String.format("Highscore: %s", Score.getHighScore()), 700, 50);

		// Buffer aufrufen
		if (!EasterEggGUI.buffer.contentsLost())
			EasterEggGUI.buffer.show();

		// Alt-Daten entfernen
		if (graphics != null) {
			graphics.dispose();
		}
		if (graphics2D != null) {
			graphics2D.dispose();
		}
		if (graphicsTenbusch != null) {
			graphicsTenbusch.dispose();
		}
	}

	/*
	 * Berechnet die FPS und sorgt fuer den "Spielfluss"
	 */
	public void gameloop() {
		long lastLooptime = System.nanoTime();
		long lastFPSupdate = 0;
		int i = 0;
		while (!beenden) {
			while (!pause) {
				long now = System.nanoTime();
				long updateCounter = now - lastLooptime;
				lastLooptime = now;
				lastFPSupdate += updateCounter;
				FPS.fps++;

				this.render(lastFPSupdate);

				if (lastFPSupdate >= 1000000000) {
					FPS.fps = 0;
					lastFPSupdate = 0;
					i++;

				}
				if (i == 2) {
					ControllerEasterEgg.erstellePipe();
					i = 0;
				}
				if (ControllerEasterEgg.hintergrund1.getPosX() <= 0
						- ControllerEasterEgg.hintergrund1.getHintergrundBild().getWidth(null)) {
					ControllerEasterEgg.hintergrund1
							.setPosX(ControllerEasterEgg.hintergrund2.getHintergrundBild().getWidth(null)
									+ ControllerEasterEgg.hintergrund2.getPosX());
				}
				if (ControllerEasterEgg.hintergrund2.getPosX() <= 0
						- ControllerEasterEgg.hintergrund2.getHintergrundBild().getWidth(null)) {
					ControllerEasterEgg.hintergrund2
							.setPosX(ControllerEasterEgg.hintergrund1.getHintergrundBild().getWidth(null)
									+ ControllerEasterEgg.hintergrund1.getPosX());
				}
				for (int j = 0; j < ControllerEasterEgg.pipes.size(); j++) {
					if (ControllerEasterEgg.pipes.get(j).getPosX() >= ControllerEasterEgg.tenbusch.getPosX()
							&& ControllerEasterEgg.pipes.get(j).getPosX() <= ControllerEasterEgg.tenbusch.getPosX()
									+ ControllerEasterEgg.tenbusch.getWidth() - 20) {
						Score.setScore(j / 2 + 1);
						if (Score.getScore() > Score.getHighScore()) {
							Score.setHighScore(Score.getScore());
						}
					}
				}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			lastFPSupdate = 0;
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	// Methode zum Starten des Threads, fuer externe Klassen
	public void start() {
		gameScreenThread.start();
	}

	// run-Methode des Threads
	@Override
	public void run() {
		this.gameloop();
	}
}
