Create table T_Benutzer
(
P_Name VARCHAR(20) NOT NULL,
PRIMARY KEY (P_Name)
);
	
Create table T_Termin
(
P_ID INT AUTO_INCREMENT,
Terminname VARCHAR(100),
Datum VARCHAR(50) NOT NULL,
Erledigt boolean,
Kurzbeschreibung VARCHAR(40),
Beschreibung TEXT,
PRIMARY KEY (P_ID)
);

Create table T_Erinnerung
(
P_Zeitpunkt VARCHAR(50),
P_F_ID INT,
PRIMARY KEY (P_Zeitpunkt, P_F_ID),
FOREIGN KEY (P_F_ID) REFERENCES T_Termin (P_ID) ON DELETE CASCADE
);

Create table T_Benutzer_Termin
(
PF_Name VARCHAR(20),
PF_ID INT,
PRIMARY KEY (PF_Name, PF_ID),
FOREIGN KEY (PF_Name) REFERENCES T_Benutzer (P_Name) ON DELETE CASCADE,
FOREIGN KEY (PF_ID) REFERENCES T_Termin (P_ID) ON DELETE CASCADE
);