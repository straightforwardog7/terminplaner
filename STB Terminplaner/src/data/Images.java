package data;

import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class Images {

	public static BufferedImage logo_STB;
	public static ImageIcon logo_STB_animiert;
	public static BufferedImage checkbox_clicked;
	public static BufferedImage checkbox_unclicked;
	public static BufferedImage einloggen_button;
	public static BufferedImage terminEintragen_button;
	public static BufferedImage eintragen_button;
	public static BufferedImage speichern_button;
	
}
