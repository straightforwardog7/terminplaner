package data;

import model.Benutzer;
import model.Erinnerung;
import model.Termin;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;



public class Database {
	
	private final String path = "jdbc:sqlite:"+ System.getProperty("user.home") + "/" + "Terminplaner.db";
	private Connection con;

	/**
	 * Erstellt die Datenbank inkl. Tabellen
	 */
	public Database() {
		if (!new File(System.getProperty("user.home") + "/" + "Terminplaner.db").exists()) {
			erstelleConnection();
			try {
				String t1 = "CREATE TABLE T_Benutzer (P_Name VARCHAR(20) NOT NULL, PRIMARY KEY (P_Name));";
				String t2 = "CREATE TABLE T_Termin (P_ID INTEGER PRIMARY KEY AUTOINCREMENT, Terminname VARCHAR(100), Datum VARCHAR(50) NOT NULL, Erledigt BOOLEAN, Kurzbeschreibung VARCHAR(40), Beschreibung TEXT, Haeufigkeit INTEGER, ArtHaeufigkeit INTEGER, Benachrichtigt BOOLEAN);";
				String t3 = "CREATE TABLE T_Erinnerung ( P_Zeitpunkt VARCHAR(50), PF_ID INT, Benachrichtigt BOOLEAN, PRIMARY KEY (P_Zeitpunkt, PF_ID), FOREIGN KEY (PF_ID) REFERENCES T_Termin (P_ID) ON DELETE CASCADE);";
				String t4 = "CREATE TABLE T_Benutzer_Termin ( PF_Name VARCHAR(20), PF_ID INT, PRIMARY KEY (PF_Name, PF_ID), FOREIGN KEY (PF_Name) REFERENCES T_Benutzer (P_Name) ON DELETE CASCADE, FOREIGN KEY (PF_ID) REFERENCES T_Termin (P_ID) ON DELETE CASCADE);";
				PreparedStatement ps = con.prepareStatement(t1);
				ps.executeUpdate();
				PreparedStatement ps2 = con.prepareStatement(t2);
				ps2.executeUpdate();
				PreparedStatement ps3 = con.prepareStatement(t3);
				ps3.executeUpdate();
				PreparedStatement ps4 = con.prepareStatement(t4);
				ps4.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Erstellt die Datenbank mit vorgefertigten Daten (fuer Import)
	 *
	 * @param benutzerListe Liste mit Benutzern
	 * @param terminListe Liste mit exportierten Terminen
	 */
	public Database(List<Benutzer> benutzerListe, List<Termin> terminListe) {
		erstelleConnection();
		for (Benutzer b : benutzerListe) {
			benutzerEintragen(b);
		}
		for (Termin t : terminListe) {
			terminEintragen(t);

		}
	}


	/**
	 * Erstellt eine Verbindung zur Datenbank
	 */
	private void erstelleConnection() {
		try {
			if (con == null || con.isClosed()) {
				con = DriverManager.getConnection(path);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Traegt den uebergebenen Benutzer in die Datenbank ein
	 * @param b Nutzer, der eingetragen werden soll
	 * @return Erfolg oder Misserfolg
	 */
	public boolean benutzerEintragen(Benutzer b) {
		erstelleConnection();
		try {
			String sql = "Insert into T_Benutzer (P_Name) values " + "(?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, b.getName());
			ps.executeUpdate();
			ps.close();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Ermoeglicht das Eintragen eines Termins
	 * @param t Termin, der eingetragen werden soll
	 * @return Erfolg oder Misserfolg
	 */
	public boolean terminEintragen(Termin t) {

		erstelleConnection();
		try {
			String sql;
			sql = "INSERT INTO T_Termin (P_ID, Terminname, Datum, Erledigt, Kurzbeschreibung, Beschreibung," +
					" Haeufigkeit, ArtHaeufigkeit, Benachrichtigt) VALUES " + "(?, ?, ?, ?, ?, ?, ?, ?, ?);";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, t.getId());
			ps.setString(2, t.getTerminName());
			ps.setString(3, t.getZeitpunkt().toString());
			ps.setBoolean(4, t.isErledigt());
			ps.setString(5, t.getKurzBeschreibung());
			ps.setString(6, t.getBeschreibung());
			ps.setInt(7, t.getHaeufigkeit());
			ps.setInt(8, t.getArtHaeufigkeit());
			ps.setBoolean(9, t.isBenachrichtigt());
			ps.executeUpdate();

			int id = t.getId();
			for (int i = 0; i < t.getTeilnehmer().size(); i++) {
				sql = "INSERT INTO T_Benutzer_Termin (PF_Name, PF_ID)" + "VALUES (?,?)";
				ps = con.prepareStatement(sql);
				ps.setString(1, t.getTeilnehmer().get(i).getName());
				ps.setInt(2, id);
				ps.executeUpdate();
			}
			for (int i = 0; i < t.getErinnerungsZeitpunkte().size(); i++) {
				sql = "INSERT INTO T_Erinnerung (P_Zeitpunkt, PF_ID, Benachrichtigt)" + "VALUES (?,?,?)";
				ps = con.prepareStatement(sql);
				ps.setString(1, t.getErinnerungsZeitpunkte().get(i).toString());
				ps.setInt(2, id);
				ps.setBoolean(3, t.getErinnerungsZeitpunkte().get(i).isBenachrichtigt());
				ps.executeUpdate();
			}


			return true;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Ruft alle Benutzer aus der DB
	 * @return eine Liste aller Benutzer
	 */
	public List<Benutzer> getAlleBenutzer() {
		List<Benutzer> benutzerliste = new ArrayList<>();
		erstelleConnection();
		try {
			String sql = "SELECT * FROM T_Benutzer;";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while (rs.next()) {
				String name = rs.getString("P_Name");
				benutzerliste.add(new Benutzer(name));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return benutzerliste;
	}

	/**
	 * Gibt alle Termine in der Datenbank zurueck
	 * @return alle Termine in der DB
	 */
	public List<Termin> getAlleTermine() {
		List<Termin> terminListe = new ArrayList<>();
		erstelleConnection();
		try {
			String sql = "SELECT * " +
					"FROM T_Termin ";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("P_ID");
				String name = rs.getString("Terminname");
				LocalDateTime datum = LocalDateTime.parse(rs.getString("Datum"));
				boolean erledigt = rs.getBoolean("Erledigt");
				String kurzbeschreibung = rs.getString("Kurzbeschreibung");
				String beschreibung = rs.getString("Beschreibung");
				List<Benutzer> teilnehmer = getTeilnehmer(id);
				List<Erinnerung> erinnerungen = getErinnerungen(id);
				int haeufigkeit = rs.getInt("Haeufigkeit");
				int artHaeufigkeit = rs.getInt("ArtHaeufigkeit");
				boolean benachrichtigt = rs.getBoolean("Benachrichtigt");

				terminListe.add(new Termin(id, name, kurzbeschreibung, beschreibung, datum, teilnehmer, erinnerungen,
						erledigt, haeufigkeit, artHaeufigkeit, benachrichtigt));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return terminListe;
	}

	/**
	 * Ruft eine Liste von Terminen ab, die den Benutzer betreffen
	 *
	 * @param b Benutzer, dessen Termine abgerufen werden sollen
	 * @param erledigteTermineAnzeigen regelt, ob bereits erledigte Termine ausgelesen werden sollen
	 * @return eine Liste von Terminen des Benutzers
	 */
	public List<Termin> getTermine(Benutzer b, boolean erledigteTermineAnzeigen) {
		List<Termin> termine = new ArrayList<>();
		erstelleConnection();

		try {
			String sql = "SELECT * " +
					"FROM T_Termin " +
					"INNER JOIN T_Benutzer_Termin ON T_Termin.P_ID = T_Benutzer_Termin.PF_ID " +
					"WHERE T_Benutzer_Termin.PF_Name='" + b.getName() + "'";

			if (!erledigteTermineAnzeigen) {
				sql += " AND Erledigt = 0";
			} else {
				sql += ";";
			}

			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while (rs.next()) {
				int id = rs.getInt("P_ID");
				String name = rs.getString("Terminname");
				LocalDateTime datum = LocalDateTime.parse(rs.getString("Datum"));
				boolean erledigt = rs.getBoolean("Erledigt");
				String kurzbeschreibung = rs.getString("Kurzbeschreibung");
				String beschreibung = rs.getString("Beschreibung");
				List<Benutzer> teilnehmer = getTeilnehmer(id);
				List<Erinnerung> erinnerungen = getErinnerungen(id);
				int haeufigkeit = rs.getInt("Haeufigkeit");
				int artHaeufigkeit = rs.getInt("ArtHaeufigkeit");
				boolean benachrichtigt = rs.getBoolean("Benachrichtigt");

				termine.add(new Termin(id, name, kurzbeschreibung, beschreibung, datum, teilnehmer, erinnerungen,
						erledigt, haeufigkeit, artHaeufigkeit, benachrichtigt));
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return termine;
	}

	/**
	 * Gibt ein Terminobjekt mit den Daten es Termins zurueck, der zu der uebergebenen ID gehoert
	 * @param id ID des auszulesenden Termins
	 * @return ausgelesenen Termin
	 */
	public Termin getTermin(int id) {
		erstelleConnection();
		Termin t = null;
		try {
			String sql = "Select * From T_Termin Where P_ID =" + id + ";";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while (rs.next()) {
				String name = rs.getString("Terminname");
				LocalDateTime datum = LocalDateTime.parse(rs.getString("Datum"));
				boolean erledigt = rs.getBoolean("Erledigt");
				String kurzbeschreibung = rs.getString("Kurzbeschreibung");
				String beschreibung = rs.getString("Beschreibung");
				List<Benutzer> teilnehmer = getTeilnehmer(id);
				List<Erinnerung> erinnerungen = getErinnerungen(id);
				int haeufigkeit = rs.getInt("Haeufigkeit");
				int artHaeufigkeit = rs.getInt("ArtHaeufigkeit");
				boolean benachrichtigt = rs.getBoolean("Benachrichtigt");

				t = new Termin(id, name, kurzbeschreibung, beschreibung, datum, teilnehmer, erinnerungen,
						erledigt, haeufigkeit, artHaeufigkeit, benachrichtigt);
			}


		}catch (Exception e) {
			e.printStackTrace();
		}
		return t;
	}

	/**
	 * Gibt die ID des zuletzt eingetragenen Termins zurueck
	 * @return ID des zuletzt eingetragenen Termins
	 */
	public int getLastID() {
		int id = 0;
		erstelleConnection();
		try {
			String sql = "SELECT P_ID FROM T_Termin ORDER BY P_ID DESC LIMIT 1;";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);

			if (rs.next()) {
				id = rs.getInt(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	/**
	 * Gibt eine Liste von Benutzern zurueck, die der Termin mit der uebergebenen ID betrifft
	 * @param id ID des Termins, dessen Benutzer/Teilnehmer ausgelesen werden sollen
	 * @return Erfolg oder Misserfolg
	 */
	public List<Benutzer> getTeilnehmer(int id) {
		List<Benutzer> teilnehmer = new ArrayList<>();
		erstelleConnection();
		try {
			String sql = "Select PF_Name From T_Benutzer_Termin Where PF_ID =" + id + ";";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while (rs.next()) {
				String name = rs.getString(1);
				teilnehmer.add(new Benutzer(name));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teilnehmer;
	}

	/**
	 * Gibt eine Liste mit Erinnerungen fuer den Termin mit der uebergebenen ID zurueck
	 * @param id ID des Termins, dessen Erinnerungen ausgelesen werden sollen
	 * @return Erfolg oder Misserfolg
	 */
	public List<Erinnerung> getErinnerungen(int id) {
		List<Erinnerung> erinnerungen = new ArrayList<>();
		erstelleConnection();
		try {
			String sql = "SELECT P_Zeitpunkt, Benachrichtigt FROM T_Erinnerung WHERE PF_ID =" + id + ";";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while (rs.next()) {
				LocalDateTime zeitpunkt = LocalDateTime.parse(rs.getString(1));
				boolean benachrichtigt = rs.getBoolean(2);
				erinnerungen.add(new Erinnerung(zeitpunkt, benachrichtigt));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return erinnerungen;
	}

	/**
	 * Aktualisiert die Attribute "erledigt" und "benachrichtigt" des uebergebenen Termins in der DB
	 * @param termin Termin, dessen Attribute aktualisiert werden sollen
	 * @return Erfolg oder Misserfolg
	 */
	public boolean updateTermin(Termin termin) {
		erstelleConnection();

		try {
			String sql = "UPDATE T_Termin " +
					"SET Erledigt=?, Benachrichtigt=? " +
					"WHERE P_ID=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setBoolean(1, termin.isErledigt());
			ps.setBoolean(2, termin.isBenachrichtigt());
			ps.setInt(3, termin.getId());
			ps.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Loescht den Termin mit der uebergebenen ID aus der Datenbank
	 * @param id ID des zu loeschenden Termins
	 * @return Erfolg oder Misserfolg
	 */
	public boolean deleteTermin(int id) {
		erstelleConnection();
		try {
			String sql = "Delete From T_Termin Where P_ID =" + id + ";";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();

			sql = "DELETE FROM T_Benutzer_Termin WHERE PF_ID=" + id + ";";
			ps = con.prepareStatement(sql);
			ps.executeUpdate();

			sql = "DELETE FROM T_Erinnerung WHERE PF_ID=" + id + ";";
			ps = con.prepareStatement(sql);
			ps.executeUpdate();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Loescht den Benutzer mit dem jeweiligen Namen aus dem Benutzer-Objekt aus der Datenbank
	 * @param b Benutzer, der geloescht werden soll
	 * @return Erfolg oder Misserfolg
	 */
	public boolean deleteBenutzer(Benutzer b) {
		erstelleConnection();
		try {
			String sql = "Delete From T_Benutzer Where P_Name = '" + b.getName() + "' ;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Loescht die Datenbank
	 * Vorbereitung auf den Import
	 *
	 * @return Erfolg oder Misserfolg
	 */
	public boolean deleteDB() {
		try {
			String sql = "DELETE FROM T_Erinnerung;" +
					"DELETE FROM T_Benutzer_Termin;" +
					"DELETE FROM T_Benutzer;" +
					"DELETE FROM T_Termin";
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Loescht den Termin (u. Fremdschluesselbeziehungen) mit der ID,
	 * die der uebergebene Termin traegt und traegt den uebergebenen Termin ein
	 *
	 * @param termin Termin, der eingetragen werden soll
	 * @return erfolgreich (true) oder Fehler (false)
	 */
	public boolean bearbeiteTermin(Termin termin) {
		this.deleteTermin(termin.getId());

		erstelleConnection();
		try {
			String sql;
			sql = "INSERT INTO T_Termin (P_ID, Terminname, Datum, Erledigt, Kurzbeschreibung, Beschreibung," +
					" Haeufigkeit, ArtHaeufigkeit, Benachrichtigt) VALUES " + "(?, ?, ?, ?, ?, ?, ?, ?, ?);";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, termin.getId());
			ps.setString(2, termin.getTerminName());
			ps.setString(3, termin.getZeitpunkt().toString());
			ps.setBoolean(4, termin.isErledigt());
			ps.setString(5, termin.getKurzBeschreibung());
			ps.setString(6, termin.getBeschreibung());
			ps.setInt(7, termin.getHaeufigkeit());
			ps.setInt(8, termin.getArtHaeufigkeit());
			ps.setBoolean(9, termin.isBenachrichtigt());
			ps.executeUpdate();

			int id = termin.getId();
			for (int i = 0; i < termin.getTeilnehmer().size(); i++) {
				sql = "INSERT INTO T_Benutzer_Termin (PF_Name, PF_ID)" + "VALUES (?,?)";
				ps = con.prepareStatement(sql);
				ps.setString(1, termin.getTeilnehmer().get(i).getName());
				ps.setInt(2, id);
				ps.executeUpdate();
			}
			for (int i = 0; i < termin.getErinnerungsZeitpunkte().size(); i++) {
				sql = "INSERT INTO T_Erinnerung (P_Zeitpunkt, PF_ID, Benachrichtigt)" + "VALUES (?,?,?)";
				ps = con.prepareStatement(sql);
				ps.setString(1, termin.getErinnerungsZeitpunkte().get(i).toString());
				ps.setInt(2, id);
				ps.setBoolean(3, termin.getErinnerungsZeitpunkte().get(i).isBenachrichtigt());
				ps.executeUpdate();
			}


			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
}
