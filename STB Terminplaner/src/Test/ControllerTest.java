package Test;

import logic.Controller;
import model.Benutzer;
import model.Erinnerung;
import model.Termin;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ControllerTest {

	public static void main(String[] args) {
		Controller.benutzerEintragen("Bernd");
		Controller.benutzerEintragen("Horst");
		Controller.benutzerEintragen("Günter");
		Controller.benutzerEintragen("Weiblicher Günter");

		Benutzer b1 = new Benutzer("Bernd");
		Benutzer b2 = new Benutzer("Horst");
		Benutzer b3 = new Benutzer("Günter");
		Benutzer b4 = new Benutzer("Weiblicher Günter");

		Erinnerung e1 = new Erinnerung(LocalDateTime.now().minusHours(12), false);
		Erinnerung e2 = new Erinnerung(LocalDateTime.now().minusHours(6), false);
		Erinnerung e3 = new Erinnerung(LocalDateTime.now().minusMinutes(12), false);
		Erinnerung e4 = new Erinnerung(LocalDateTime.now().minusHours(5), false);


		/*Termin termin1 = new Termin(1, "Erster Termin", "Hallo", "Welt",
				LocalDateTime.now().minusDays(1L), new ArrayList<>(Arrays.asList(b1, b2, b3)),
				new ArrayList<>(Arrays.asList(e1, e2)), false);

		Termin termin2 = new Termin(2, "Zweiter Termin", "Tschüss", "Welt",
				LocalDateTime.now().minusDays(1L), new ArrayList<>(Collections.singletonList(b4)),
				new ArrayList<>(Collections.singletonList(e3)), false);*/

		/*TerminRepeat termin3 = new TerminRepeat(3, "Dritter Termin", "Wieder", "da",
				LocalDateTime.now().minusWeeks(10L), new ArrayList<>(Collections.singleton(b3)),
				new ArrayList<>(Collections.singleton(e4)), false, 2, TerminRepeat.ALLE_MONATE);*/

		Controller.terminEintragen("Erster Termin", "Hallo", "Welt",
				LocalDateTime.now().minusDays(1L), new ArrayList<>(Arrays.asList(b1, b2, b3)),
				new ArrayList<>(Arrays.asList(e1, e2)), false);

		Controller.terminEintragen("Zweiter Termin", "Tschüss", "Welt",
				LocalDateTime.now().minusDays(1L), new ArrayList<>(Collections.singletonList(b4)),
				new ArrayList<>(Collections.singletonList(e3)), false);

		Controller.terminEintragen("Dritter Termin", "Wieder", "da",
				LocalDateTime.now().minusWeeks(10L), new ArrayList<>(Collections.singleton(b3)),
				new ArrayList<>(Collections.singleton(e4)), 2, Termin.ALLE_MONATE, false);

		List<Termin> terminListeVorher = Controller.getAlleTermine();
		terminListeVorher.forEach(System.out::println);

		Controller.exportieren(new File("hallo.stbt"));

		Controller.importieren(new File("hallo.stbt"));

		List<Termin> terminListeNachher = Controller.getAlleTermine();
		terminListeNachher.forEach(System.out::println);

		Controller.login(b1.getName());
		List<Termin> b1Termine = Controller.getTermine();
		System.out.println("");
		b1Termine.forEach(System.out::println);
	}
}
