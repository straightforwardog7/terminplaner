package Test;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import model.Termin;

import java.io.IOException;
import java.util.List;

public class TestJSON {

	public static void main(String[] args) {
		String json = "[ {\n" +
				"  \"id\" : 1,\n" +                        //Anfang erster Termin
				"  \"terminName\" : \"Erster Termin\",\n" +
				"  \"kurzBeschreibung\" : \"Hallo\",\n" +
				"  \"beschreibung\" : \"Welt\",\n" +
				"  \"zeitpunkt\" : \"2019-11-27T23:44:03.215\",\n" +
				"  \"teilnehmer\" : [ {\n" +
				"    \"name\" : \"Bernd\"\n" +
				"  }, {\n" +
				"    \"name\" : \"Horst\"\n" +
				"  }, {\n" +
				"    \"name\" : \"Günter\"\n" +
				"  } ],\n" +
				"  \"erinnerungsZeitpunkte\" : [ {\n" +
				"    \"erinnerungsZeitpunkt\" : \"2019-11-28T11:44:03.215\"\n" +
				"  }, {\n" +
				"    \"erinnerungsZeitpunkt\" : \"2019-11-28T17:44:03.215\"\n" +
				"  } ],\n" +
				"  \"erledigt\" : false,\n" +
				"  \"haeufigkeit\" : -1,\n" +
				"  \"artHaeufigkeit\" : -1\n" +
				"}, {\n" +                                    //Ende erster Termin
				"  \"id\" : 2,\n" +                                //Anfang zweiter Termin
				"  \"terminName\" : \"Zweiter Termin\",\n" +
				"  \"kurzBeschreibung\" : \"Tschüss\",\n" +
				"  \"beschreibung\" : \"Welt\",\n" +
				"  \"zeitpunkt\" : \"2019-11-27T23:44:03.949\",\n" +
				"  \"teilnehmer\" : [ {\n" +
				"    \"name\" : \"Weiblicher Günter\"\n" +
				"  } ],\n" +
				"  \"erinnerungsZeitpunkte\" : [ {\n" +
				"    \"erinnerungsZeitpunkt\" : \"2019-11-28T23:32:03.215\"\n" +
				"  } ],\n" +
				"  \"erledigt\" : false,\n" +
				"  \"haeufigkeit\" : -1,\n" +
				"  \"artHaeufigkeit\" : -1\n" +
				"}, {\n" +                                    //Ende zweiter Termin
				"  \"id\" : 3,\n" +                        //Anfang dritter Termin
				"  \"terminName\" : \"Dritter Termin\",\n" +
				"  \"kurzBeschreibung\" : \"Wieder\",\n" +
				"  \"beschreibung\" : \"da\",\n" +
				"  \"zeitpunkt\" : \"2019-09-19T23:44:04.324\",\n" +
				"  \"teilnehmer\" : [ {\n" +
				"    \"name\" : \"Günter\"\n" +
				"  } ],\n" +
				"  \"erinnerungsZeitpunkte\" : [ {\n" +
				"    \"erinnerungsZeitpunkt\" : \"2019-11-28T18:44:03.215\"\n" +
				"  } ],\n" +
				"  \"erledigt\" : false,\n" +
				"  \"haeufigkeit\" : 2,\n" +
				"  \"artHaeufigkeit\" : 2\n" +
				"} ]";                                        //Ende dritter Termin

		List<Termin> terminliste = termineUmwandeln(json);

		terminliste.forEach(System.out::println);
	}

	public static List<Termin> termineUmwandeln(String json) {
		JsonFactory jsonFactory = new JsonFactory();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		List<Termin> terminListe;

		try {
			JsonParser jsonParser = jsonFactory.createParser(json);
			terminListe = objectMapper.readValue(jsonParser, new TypeReference<List<Termin>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return terminListe;
	}
}
