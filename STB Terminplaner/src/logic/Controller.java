package logic;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import data.Database;
import model.Benutzer;
import model.Erinnerung;
import model.Termin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class Controller {

	private static Database database = new Database();
	private static Benutzer eingeloggterBenutzer;
	public static int idAlt = -1;
	public static boolean bearbeiten = false;
	private static boolean erledigteTermineAnzeigen = true;
	private static int anzahlWiederkehrendeTermine = 1;

	public static boolean login(String name) {
		Benutzer neuerBenutzer = new Benutzer(name);

		if (database.getAlleBenutzer().contains(neuerBenutzer)) {
			eingeloggterBenutzer = neuerBenutzer;
			getTermine();
			return true;
		} else {
			return false;
		}
	}

	public static void benutzerLoeschen(String name) {
		database.deleteBenutzer(new Benutzer(name));
	}

	public static Benutzer getEingeloggterBenutzer() {
		return eingeloggterBenutzer;
	}

	public static void logout() {
		Controller.eingeloggterBenutzer = null;
	}

	public static boolean benutzerEintragen(String name) {
		return database.benutzerEintragen(new Benutzer(name));
	}

	/**
	 * Methode zum Eintragen eines einmaligen Termins
	 *
	 * @param terminName            Name des Termins
	 * @param beschreibungKurz      eine kurze Beschreibung des Termins
	 * @param beschreibungLang      eine ausfuehrliche Beschreibung des Termins
	 * @param zeitpunkt             der Zeitpunkt des Termins
	 * @param teilnehmer            eine Liste von Benutzern, die der Termin betrifft
	 * @param erinnerungsZeitpunkte eine Liste von Zeitpunkten, an denen der/die Benutzer erinnert werden will/wollen
	 */
	public static void terminEintragen(String terminName, String beschreibungKurz, String beschreibungLang,
									   LocalDateTime zeitpunkt, List<Benutzer> teilnehmer,
									   List<Erinnerung> erinnerungsZeitpunkte, boolean benachrichtigt) {
		terminEintragen(terminName, beschreibungKurz, beschreibungLang, zeitpunkt, teilnehmer, erinnerungsZeitpunkte,
				-1, -1, benachrichtigt); //Haeufigkeit und Art der Haeufigkeit auf -1, weil einmaliger Termin
	}

	public static Termin getTermin(int id) {
		return database.getTermin(id);
	}

	public static boolean updateTermin(Termin termin) {
		return database.updateTermin(termin);
	}

	/**
	 * Methode zum Eintragen eines Termins (wiederholend und einmalig).
	 * Bevorzugt zum Eintragen von sich wiederholenden Terminen benutzen
	 *
	 * @param terminName            Name des Termins
	 * @param beschreibungKurz      eine kurze Beschreibung des Termins
	 * @param beschreibungLang      eine ausfuehrliche Beschreibung des Termins
	 * @param zeitpunkt             der Zeitpunkt des Termins
	 * @param teilnehmer            eine Liste von Benutzern, die der Termin betrifft
	 * @param erinnerungsZeitpunkte eine Liste von Zeitpunkten, an denen der/die Benutzer erinnert werden will/wollen
	 * @param haeufigkeit           Positive natuerliche Zahl, die die Haeufigkeit des Termins angibt. Wenn -1, einmaliger Termin
	 * @param haeufigkeitArt        gibt mithilfe von Termin.ALLE_TAGE, Termin.ALLE_WOCHEN, Termin.ALLE_MONATE,
	 *                              Termin.ALLE_JAHRE die Art der Haeufigkeit an
	 */
	public static void terminEintragen(String terminName, String beschreibungKurz, String beschreibungLang,
									   LocalDateTime zeitpunkt, List<Benutzer> teilnehmer,
									   List<Erinnerung> erinnerungsZeitpunkte, int haeufigkeit,
									   int haeufigkeitArt, boolean benachrichtigt) {
		Termin termin;
		int id;
		if (bearbeiten && idAlt != -1) {
			id = idAlt;
		} else {
			id = database.getLastID() + 1;
		}
		if (haeufigkeit == -1 || haeufigkeitArt == -1) { //kein sich wiederholender Termin
			termin = new Termin(id, terminName, beschreibungKurz, beschreibungLang, zeitpunkt, teilnehmer,
					erinnerungsZeitpunkte, false, benachrichtigt); //nutze Konstruktor fuer einmaligen Termin
		} else { //Termin, der sich wiederholt
			termin = new Termin(id, terminName, beschreibungKurz, beschreibungLang, zeitpunkt, teilnehmer,
					erinnerungsZeitpunkte, false, haeufigkeit, haeufigkeitArt, benachrichtigt); //nutze Konstruktor fuer sich
			// wiederholenden Termin
		}

		if (bearbeiten) {
			idAlt = -1;
			bearbeiten = false;
			database.bearbeiteTermin(termin);
		} else {
			database.terminEintragen(termin);
		}
	}

	public static boolean bearbeiteTermin(Termin termin) {
		return database.bearbeiteTermin(termin);
	}

	public static boolean loescheTermin(int termin_Id) {
		return database.deleteTermin(termin_Id);
	}

	public static List<Benutzer> getAlleBenutzer() {
		return database.getAlleBenutzer();
	}

	public static List<Termin> getTermine() {

		List<Termin> terminListe = new ArrayList<>();

		if (eingeloggterBenutzer == null) {
			return terminListe;
		}

		List<Termin> datenbankTermine = database.getTermine(eingeloggterBenutzer, erledigteTermineAnzeigen);

		for (Termin t : datenbankTermine) {
			if (t.getArtHaeufigkeit() == -1) {
				terminListe.add(t);
			} else {
				terminListe.addAll(berechneZukuenftigeTermine(t, anzahlWiederkehrendeTermine));
			}
		}

		return sortiereTermine(terminListe);
	}

	private static List<Termin> sortiereTermine(List<Termin> terminListe) {
		terminListe.sort((termin1, termin2) -> {
			if (termin1.getZeitpunkt().isEqual(termin2.getZeitpunkt())) {
				return 0;
			} else if (termin1.getZeitpunkt().isAfter(termin2.getZeitpunkt())) {
				return 1;
			} else {
				return -1;
			}
		});
		return terminListe;
	}

	private static List<Termin> berechneZukuenftigeTermine(Termin termin, int anzahlWiederkehrendeTermine) {
		List<Termin> zukuenftigeTermine = new ArrayList<>();
		zukuenftigeTermine.add(termin);

		for (int i = 0; i < anzahlWiederkehrendeTermine; i++) {
			Termin vorherigerTermin = zukuenftigeTermine.get(zukuenftigeTermine.size() - 1);

			LocalDateTime alterZeitpunkt = vorherigerTermin.getZeitpunkt();
			LocalDateTime neuerZeitPunkt = alterZeitpunkt.plus(vorherigerTermin.getHaeufigkeit(), getChronoUnit(vorherigerTermin));

			List<Erinnerung> neueErinnerungen = new ArrayList<>();

			for (Erinnerung e : vorherigerTermin.getErinnerungsZeitpunkte()) {
				LocalDateTime alterErinnerungsZeitpunkt = e.getErinnerungsZeitpunkt();
				LocalDateTime neuerErinnerungsZeitpunkt = alterErinnerungsZeitpunkt.plus(vorherigerTermin.getHaeufigkeit(), getChronoUnit(vorherigerTermin));
				neueErinnerungen.add(new Erinnerung(neuerErinnerungsZeitpunkt, false));
			}

			Termin neuerTermin = new Termin(termin.getId(), termin.getTerminName(), termin.getKurzBeschreibung(), termin.getBeschreibung(),
					neuerZeitPunkt, termin.getTeilnehmer(), neueErinnerungen, false, termin.getHaeufigkeit(), termin.getArtHaeufigkeit(), false);

			zukuenftigeTermine.add(neuerTermin);
		}

		return zukuenftigeTermine;

	}

	private static ChronoUnit getChronoUnit(Termin termin) {
		if (termin.getArtHaeufigkeit() == 0) {
			return ChronoUnit.DAYS;
		} else if (termin.getArtHaeufigkeit() == 1) {
			return ChronoUnit.WEEKS;
		} else if (termin.getArtHaeufigkeit() == 2) {
			return ChronoUnit.MONTHS;
		} else if (termin.getArtHaeufigkeit() == 3) {
			return ChronoUnit.YEARS;
		} else {
			return null;
		}
	}

	/**
	 * Importiert Datei aus der angegebenen Datei
	 *
	 * @param dateiOrt Datei, die importiert werden soll
	 * @return Erfolg oder Misserfolg des Imports
	 */
	public static boolean importieren(File dateiOrt) {
		String[] dateiInhalt = dateiEinlesen(dateiOrt);

		if (dateiInhalt == null || dateiInhalt[0] == null || dateiInhalt[1] == null) { //Wenn null, dann Fehler beim Einlesen
			return false;
		}

		eingeloggterBenutzer = null; //bei Import kein eingeloggter Nutzer

		List<Benutzer> benutzerListe = benutzerInterpretieren(dateiInhalt[0]);
		List<Termin> terminListe = termineInterpretieren(dateiInhalt[1]);

		database.deleteDB();
		database = new Database(benutzerListe, terminListe); //Erstelle neue Datenbank mit neuen Daten
		return true;
	}

	public static boolean exportieren(File speicherOrt) {
		String[] dateiInhalt = new String[2];

		dateiInhalt[0] = benutzerUmwandeln();
		dateiInhalt[1] = termineUmwandeln();

		if (dateiInhalt[1] == null) {
			return false;
		}

		return dateiSpeichern(speicherOrt, dateiInhalt);
	}

	private static boolean dateiSpeichern(File speicherOrt, String[] dateiInhalt) {
		try {
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(speicherOrt));

			//Benutzer in .STBT-Archiv exportieren
			ZipEntry benutzer = new ZipEntry("Benutzer");
			zos.putNextEntry(benutzer);
			byte[] bytes = dateiInhalt[0].getBytes();
			zos.write(bytes, 0, bytes.length);
			zos.closeEntry();

			//Termine in .STBT-Archiv exportieren
			ZipEntry termine = new ZipEntry("Termine");
			zos.putNextEntry(termine);
			bytes = dateiInhalt[1].getBytes();
			zos.write(bytes, 0, bytes.length);
			zos.closeEntry();

			zos.close();


		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	private static String benutzerUmwandeln() {
		List<Benutzer> benutzerListe = database.getAlleBenutzer();
		StringBuilder benutzerInDatei = new StringBuilder();

		for (Benutzer benutzer : benutzerListe) {
			benutzerInDatei.append(benutzer.getName()).append("\n");
		}

		return benutzerInDatei.toString();
	}

	private static String termineUmwandeln() {
		List<Termin> terminListe = database.getAlleTermine();
		Writer terminInDatei = new StringWriter();
		JsonFactory jsonFactory = new JsonFactory();

		try (JsonGenerator jGen = jsonFactory.createGenerator(terminInDatei)) {

			jGen.useDefaultPrettyPrinter(); //TODO: Schoenere Anzeige des JSON-Codes zum Debuggen
			jGen.writeStartArray();
			//Exportieren der einzelnen Termine
			for (Termin t : terminListe) {
				jGen.writeStartObject();
				jGen.writeNumberField("id", t.getId());
				jGen.writeStringField("terminName", t.getTerminName());
				jGen.writeStringField("kurzBeschreibung", t.getKurzBeschreibung());
				jGen.writeStringField("beschreibung", t.getBeschreibung());
				jGen.writeStringField("zeitpunkt", t.getZeitpunkt().toString());

				//Exportieren der Benutzer pro Termin
				jGen.writeFieldName("teilnehmer");
				jGen.writeStartArray();
				for (Benutzer b : t.getTeilnehmer()) {
					jGen.writeStartObject();
					jGen.writeStringField("name", b.getName());
					jGen.writeEndObject();
				}
				jGen.writeEndArray();

				//Exportieren der Erinnerungszeitpunkte pro Termin
				jGen.writeFieldName("erinnerungsZeitpunkte");
				jGen.writeStartArray();
				for (Erinnerung e : t.getErinnerungsZeitpunkte()) {
					jGen.writeStartObject();
					jGen.writeStringField("erinnerungsZeitpunkt", e.getErinnerungsZeitpunkt().toString());
					jGen.writeBooleanField("benachrichtigt", e.isBenachrichtigt());
					jGen.writeEndObject();
				}
				jGen.writeEndArray();

				jGen.writeBooleanField("erledigt", t.isErledigt());
				jGen.writeNumberField("haeufigkeit", t.getHaeufigkeit());
				jGen.writeNumberField("artHaeufigkeit", t.getArtHaeufigkeit());
				jGen.writeBooleanField("benachrichtigt", t.isBenachrichtigt());
				jGen.writeEndObject();
			}
			jGen.writeEndArray();

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return terminInDatei.toString();
	}

	/**
	 * Liest die übergebene .STBT-Datei (komprimiert) ein und dekomprimiert sie
	 *
	 * @param dateiOrt Datei, die eingelesen werden soll
	 * @return Stringarray mit Dateiinhalt: [0] Benutzerliste; [1] Terminliste
	 */
	private static String[] dateiEinlesen(File dateiOrt) {
		ZipFile datei;
		String[] dateiInhalt = new String[2];

		try {
			datei = new ZipFile(dateiOrt);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		//Benutzer-Datei einlesen
		dateiInhalt[0] = zipEntryEinlesen(datei, datei.getEntry("Benutzer"));

		//Termine-Datei einlesen
		dateiInhalt[1] = zipEntryEinlesen(datei, datei.getEntry("Termine"));

		return dateiInhalt;
	}

	/**
	 * Liest ein Zip-Entry bzw. eine Datei eines Zip-Archives ein
	 *
	 * @param datei .STBT-Archiv, in der die noetige Datei steckt
	 * @param zipEntry Name der Datei im Archiv, die ausgelesen werden soll
	 * @return Dateiinhalt der auszulesenden Datei
	 */
	private static String zipEntryEinlesen(ZipFile datei, ZipEntry zipEntry) {
		InputStream inputStream;
		String inhaltText = null;

		try {
			inputStream = datei.getInputStream(zipEntry);

			byte[] inhalt = new byte[(int) zipEntry.getSize()];
			int geleseneBytes = 0;
			while (geleseneBytes < inhalt.length && geleseneBytes != -1) {
				geleseneBytes += inputStream.read(inhalt, geleseneBytes, inhalt.length - geleseneBytes);
			}

			inhaltText = new String(inhalt, StandardCharsets.UTF_8);

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return inhaltText;
	}

	/**
	 * Erstellt aus der ausgelesenen Namensliste eine Liste von Benutzerobjekten
	 *
	 * @param dateiInhalt Namensliste durch Zeilenumbrueche getrennt
	 * @return nutzbare Benutzerliste
	 */
	private static List<Benutzer> benutzerInterpretieren(String dateiInhalt) {
		List<Benutzer> benutzerListe = new ArrayList<>();

		String[] einzelneBenutzer = dateiInhalt.split("\\r?\\n"); //Zerlegen der Liste in einzelne Zeilen/Namen

		for (String benutzerName : einzelneBenutzer) { //Fuer jeden Benutzernamen ein Objekt erstellen
			benutzerListe.add(new Benutzer(benutzerName));
		}

		return benutzerListe; //Fertige Benutzerliste zurueckgeben
	}

	private static List<Termin> termineInterpretieren(String dateiInhalt) {
		JsonFactory jsonFactory = new JsonFactory();
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		List<Termin> terminListe;

		try {
			JsonParser jsonParser = jsonFactory.createParser(dateiInhalt);
			terminListe = objectMapper.readValue(jsonParser, new TypeReference<List<Termin>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return terminListe;
	}

	public static List<Termin> getAlleTermine() {
		return database.getAlleTermine();
	}

	public static int getIDdesAnstehendenTermins() {
		if (getEingeloggterBenutzer() != null) {
			List<Termin> terminListe = getTermine();
			LocalDateTime jetzt = LocalDateTime.now();

			for (Termin termin : terminListe) {
				if (termin.getZeitpunkt().isBefore(jetzt)) {
					continue;
				}
				return termin.getId();
			}

		}
		return -1;
	}

	public static boolean isErledigteTermineAnzeigen() {
		return erledigteTermineAnzeigen;
	}

	public static void setErledigteTermineAnzeigen(boolean erledigteTermineAnzeigen) {
		Controller.erledigteTermineAnzeigen = erledigteTermineAnzeigen;
	}

	public static int getAnzahlWiederkehrendeTermine() {
		return anzahlWiederkehrendeTermine;
	}

	public static void setAnzahlWiederkehrendeTermine(int anzahlWiederkehrendeTermine) {
		Controller.anzahlWiederkehrendeTermine = anzahlWiederkehrendeTermine;
	}
}
