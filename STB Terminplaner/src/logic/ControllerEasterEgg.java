package logic;

import java.awt.Image;
import java.util.ArrayList;

import model.Hintergrund;
import model.Pipe;
import model.Tenbusch;
import utils.Score;
import guiEasterEgg.EasterEggGUI;

public class ControllerEasterEgg {
	
	public static ArrayList<Pipe> pipes = new ArrayList<>();
	public static Tenbusch tenbusch = new Tenbusch(110, 100);
	public static int HINTERGRUND_BREITE = 6313;
	public static Hintergrund hintergrund1 = new Hintergrund(0);
	public static Hintergrund hintergrund2 = new Hintergrund(HINTERGRUND_BREITE);
	
	
	public ArrayList<Pipe> getPipes() {
		return pipes;
	}

	public void setPipes(ArrayList<Pipe> pipes) {
		ControllerEasterEgg.pipes = pipes;
	}

	public ControllerEasterEgg(ArrayList<Pipe> pipes) {
		super();
		ControllerEasterEgg.pipes = pipes;
	}

	public static void erstellePipe() {
		Pipe p1 = new Pipe(1400, 0, true);	
		p1.getPipe().getScaledInstance(100, -500,Image.SCALE_SMOOTH);
		pipes.add(p1);		
		pipes.get(pipes.indexOf(p1)).setPosY(berechnePipePosition(pipes.get(pipes.indexOf(p1))));
		Pipe p2 = new Pipe(1400, p1.getPosY() + 800, false);
		pipes.add(p2);
	}
	
	public static int berechnePipePosition(Pipe pipe){
		int posY = 0;
		if(pipes.indexOf(pipe) != 0) {
			posY = (int) (Math.random() * (-400));
		}
		return posY;
	}
	public static void gameOver() {
		
		Score.setScore(0);
		pipes.clear();
		hintergrund1.setPosX(0);
		hintergrund2.setPosX(HINTERGRUND_BREITE);
		tenbusch.setPosY(300);
		EasterEggGUI.gameScreen.setPause(true);
	}
	public static void sprung() {
		tenbusch.setGeschwindigkeit(15);
	}
	
}
