package logic;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import data.Images;

public class ImageReader {
	
	public void readImages() {
		try {
			Images.logo_STB = ImageIO.read(getClass().getResource("/pictures/STB_Icon.png"));
			Images.logo_STB_animiert = new ImageIcon(getClass().getResource("/pictures/STB_Logo_animiert.gif"));
			Images.checkbox_clicked = ImageIO.read(getClass().getResource("/pictures/checkbox_clicked.png"));
			Images.checkbox_unclicked = ImageIO.read(getClass().getResource("/pictures/checkbox_unclicked.png"));
			Images.einloggen_button = ImageIO.read(getClass().getResource("/pictures/einloggen_button.png"));
			Images.terminEintragen_button = ImageIO.read(getClass().getResource("/pictures/terminEintragen_button.png"));
			Images.eintragen_button = ImageIO.read(getClass().getResource("/pictures/Eintragen_button.png"));
			Images.speichern_button = ImageIO.read(getClass().getResource("/pictures/speichern_Button.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
