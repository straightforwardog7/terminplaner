package gui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class StatusColumnCellRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	@Override
	  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
	    JLabel l = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
	    DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
	    if (tableModel.getValueAt(row, 4).equals("offen")) {
	      l.setBackground(Color.red);
	    } else {
	      l.setBackground(Color.green);
	    }
	    return l;
	}
}