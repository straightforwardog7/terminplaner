package gui;

import data.Images;
import logic.Controller;
import utils.BenachrichtigungenChecker;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Login extends JFrame {

	private static final long serialVersionUID = 1L;
	
	public static BenachrichtigungenChecker checker;
	private JPanel contentPane;
	private GUI planer;
	
	public Login() {
		//Allgemeines
		setIconImage(Images.logo_STB);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2) - 600,
				((int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2) - 450, 1130, 800);
		contentPane = new JPanel(new BorderLayout());
		setContentPane(contentPane);
		
		// Anmelden Screen
		JPanel anmeldenScreen = new JPanel();
		GridBagConstraints gbcA = new GridBagConstraints();
		GridBagLayout gblA = new GridBagLayout();
		anmeldenScreen.setLayout(gblA);
		GridBagLayout gblASekundaer = new GridBagLayout();
		GridBagConstraints gbcASekundaer = new GridBagConstraints();
		JPanel anmeldeScreen_linksFueller = new JPanel();
		anmeldeScreen_linksFueller.setBackground(Color.DARK_GRAY);
		gbcA.fill = GridBagConstraints.BOTH;
		gbcA.weightx = 0.5;
		gbcA.weighty = 0.5;
		gbcA.gridheight = 3;
		gbcA.gridwidth = 1;
		gbcA.gridx = 0;
		gbcA.gridy = 0;
		gbcA.ipadx = 0;
		gbcA.ipady = 0;
		anmeldenScreen.add(anmeldeScreen_linksFueller, gbcA);
		JPanel anmeldeScreen_mitteObenFueller = new JPanel();
		anmeldeScreen_mitteObenFueller.setBackground(Color.DARK_GRAY);
		gbcA.gridheight = 1;
		gbcA.gridwidth = 1;
		gbcA.gridx = 1;
		gbcA.gridy = 0;
		anmeldenScreen.add(anmeldeScreen_mitteObenFueller, gbcA);
		JPanel anmeldeScreen_content = new JPanel();
		anmeldeScreen_content.setLayout(gblASekundaer);
		anmeldeScreen_content.setBackground(Color.DARK_GRAY);
		gbcA.gridheight = 1;
		gbcA.gridwidth = 1;
		gbcA.gridx = 1;
		gbcA.gridy = 1;
		// Content Elemente
		JPanel anmeldeScreen_content_Info = new JPanel(new BorderLayout());
		anmeldeScreen_content_Info.setBackground(Color.DARK_GRAY);
		anmeldeScreen_content_Info.setBorder(new EmptyBorder(180, 400, 0, 0));
		gbcASekundaer.fill = GridBagConstraints.BOTH;
		gbcASekundaer.weightx = 0.5;
		gbcASekundaer.weighty = 0.5;
		gbcASekundaer.gridheight = 1;
		gbcASekundaer.gridwidth = 1;
		gbcASekundaer.gridx = 0;
		gbcASekundaer.gridy = 0;
		gbcASekundaer.ipadx = 0;
		gbcASekundaer.ipady = 0;
		JLabel anmeldeScreen_lbl_Info = new JLabel("Anmelden");
		anmeldeScreen_lbl_Info.setBackground(Color.DARK_GRAY);
		anmeldeScreen_lbl_Info.setFont(anmeldeScreen_lbl_Info.getFont().deriveFont((float) 28));
		anmeldeScreen_lbl_Info.setForeground(Color.white);
		anmeldeScreen_content_Info.add(anmeldeScreen_lbl_Info, BorderLayout.CENTER);
		anmeldeScreen_content.add(anmeldeScreen_content_Info, gbcASekundaer);
		JPanel anmeldeScreen_content_loginLine = new JPanel();
		anmeldeScreen_content_loginLine.setBackground(Color.DARK_GRAY);
		GroupLayout gplA2 = new GroupLayout(anmeldeScreen_content_loginLine);
		anmeldeScreen_content_loginLine.setLayout(gplA2);
		// automatische Abstaende zum Bildschirmrand und zwischen Elementen
		gplA2.setAutoCreateGaps(true);
		gplA2.setAutoCreateContainerGaps(true);
		anmeldeScreen_content_loginLine.setBorder(new EmptyBorder(0, 310, 0, 0));
		anmeldeScreen_content_loginLine.setBackground(Color.DARK_GRAY);
		gbcASekundaer.gridheight = 1;
		gbcASekundaer.gridwidth = 1;
		gbcASekundaer.gridx = 0;
		gbcASekundaer.gridy = 1;
		JLabel anmeldeScreen_content_loginLine_Info = new JLabel("Name:");
		anmeldeScreen_content_loginLine_Info.setForeground(Color.WHITE);
		anmeldeScreen_content_loginLine_Info
				.setFont(anmeldeScreen_content_loginLine_Info.getFont().deriveFont((float) 20));
		JTextField anmeldeScreen_content_loginLine_Eingabe = new JTextField();
		// GroupLayout konfigurieren
		gplA2.setVerticalGroup(gplA2.createSequentialGroup()
				.addGroup(gplA2.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(anmeldeScreen_content_loginLine_Info, 30, 30, 30)
						.addComponent(anmeldeScreen_content_loginLine_Eingabe, 20, 30, 30)));

		gplA2.setHorizontalGroup(
				gplA2.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(gplA2.createSequentialGroup()
								.addComponent(anmeldeScreen_content_loginLine_Info, 100, 110, 110)
								.addComponent(anmeldeScreen_content_loginLine_Eingabe, 10, 200, 200)));

		anmeldeScreen_content.add(anmeldeScreen_content_loginLine, gbcASekundaer);
		JPanel anmeldeScreen_content_buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		anmeldeScreen_content_buttonPanel.setBorder(new EmptyBorder(0, 305, 0, 0));
		anmeldeScreen_content_buttonPanel.setBackground(Color.DARK_GRAY);
		gbcASekundaer.gridheight = 1;
		gbcASekundaer.gridwidth = 1;
		gbcASekundaer.gridx = 0;
		gbcASekundaer.gridy = 2;
		gbcASekundaer.ipady = 143;

		JButton anmeldeScreen_content_btn_bestaetigen = new JButton();
		anmeldeScreen_content_btn_bestaetigen.setIcon(new ImageIcon(Images.einloggen_button));
		anmeldeScreen_content_btn_bestaetigen.setBorder(null);
		anmeldeScreen_content_btn_bestaetigen.setFocusable(false);
		anmeldeScreen_content_btn_bestaetigen.setContentAreaFilled(false);
		ActionListener einloggen = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!anmeldeScreen_content_loginLine_Eingabe.getText().equals("")) {
					boolean erfolgreich = Controller.login(anmeldeScreen_content_loginLine_Eingabe.getText());
					if (erfolgreich) {
						JOptionPane.showMessageDialog(StartGUI.frame.getLoginStarter().getLogin().getPlaner(), "Sie wurden erfolgreich eingeloggt!",
								"Anmeldung erfolgreich", JOptionPane.INFORMATION_MESSAGE);
						if (planer == null) {
							planer = new GUI();
						}else {
							planer.setVisible(true);
						}
						planer.aktualisieren();
						checker = new BenachrichtigungenChecker();
						checker.start();
						getLogin().setVisible(false);
					} else {
						int reply = JOptionPane.showConfirmDialog(null, "Wollen Sie sich jetzt registrieren?", "Registrieren", JOptionPane.YES_NO_OPTION);
						if (reply == JOptionPane.YES_OPTION) {
						        	Controller.benutzerEintragen(anmeldeScreen_content_loginLine_Eingabe.getText());
						          JOptionPane.showMessageDialog(null, "Sie wurden registiert");
						        }
						        else {
						           JOptionPane.showMessageDialog(null, "Ohne uns!");
						           System.exit(0);
						        }
							}
							anmeldeScreen_content_loginLine_Eingabe.setText("");
				}
			}};
		anmeldeScreen_content_loginLine_Eingabe.addActionListener(einloggen);
		anmeldeScreen_content_btn_bestaetigen.addActionListener(einloggen);
		anmeldeScreen_content_buttonPanel.add(anmeldeScreen_content_btn_bestaetigen);
		anmeldeScreen_content.add(anmeldeScreen_content_buttonPanel, gbcASekundaer);
		anmeldenScreen.add(anmeldeScreen_content, gbcA);
		JPanel anmeldeScreen_mitteUntenFueller = new JPanel();
		anmeldeScreen_mitteUntenFueller.setBackground(Color.DARK_GRAY);
		gbcA.gridheight = 1;
		gbcA.gridwidth = 1;
		gbcA.gridx = 1;
		gbcA.gridy = 2;
		anmeldenScreen.add(anmeldeScreen_mitteUntenFueller, gbcA);
		JPanel anmeldeScreen_rechtsFueller = new JPanel();
		anmeldeScreen_rechtsFueller.setBackground(Color.DARK_GRAY);
		gbcA.gridheight = 3;
		gbcA.gridwidth = 1;
		gbcA.gridx = 2;
		gbcA.gridy = 0;
		anmeldenScreen.add(anmeldeScreen_rechtsFueller, gbcA);
		
		contentPane.add(anmeldenScreen, BorderLayout.CENTER);
		
		//Allgemeines
		setVisible(true);
	}

	private Login getLogin() {
		return this;
	}
	
	public GUI getPlaner() {
		return planer;
	}
	
}
