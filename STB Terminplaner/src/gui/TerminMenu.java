package gui;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import data.Images;
import logic.Controller;
import model.Benutzer;
import model.Termin;

class TerminMenu extends JPopupMenu {

	private static final long serialVersionUID = 1L;
	
	//Elemente
    JMenuItem bearbeiten;
    JMenuItem loeschen;
    MouseEvent ereignis;
    JMenuItem erledigt;
    int row;
    int column;
    
    //Konstruktor
    public TerminMenu(MouseEvent e, Point p) {
    	this.setEreignis(e);
    	this.setRow((int)p.getLocation().getX());
    	this.setColumn((int)p.getLocation().getY());
    	erstelleTerminMenuItems();
    }
    
    //Methode die als eine Art Konstruktor dient
	@SuppressWarnings("static-access")
    public void erstelleTerminMenuItems() {
        bearbeiten = new JMenuItem("Bearbeiten");
        add(bearbeiten);
        bearbeiten.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controller.bearbeiten = true;
				StartGUI.frame.login.getLogin().getPlaner().getModel_terminErinnerung().setRowCount(0);
				StartGUI.frame.login.getLogin().getPlaner().getModel_terminTeilnehmer().setRowCount(0);
				StartGUI.frame.login.getLogin().getPlaner().getKarten().show(StartGUI.frame.login.getLogin().getPlaner().getContentPane(), "termin");
				int id = (int) (StartGUI.frame.login.getLogin().getPlaner().getModel().getValueAt(getRow(),0));
				Controller.idAlt = id;
				Termin zwischen = Controller.getTermin(id);
				StartGUI.frame.login.getLogin().getPlaner().getTxt_terminName().setText(zwischen.getTerminName());
				StartGUI.frame.login.getLogin().getPlaner().getTxt_terminBeschreibungKurz().setText(zwischen.getKurzBeschreibung());
				StartGUI.frame.login.getLogin().getPlaner().getTxt_terminBeschreibungLang().setText(zwischen.getBeschreibung());
				StartGUI.frame.login.getLogin().getPlaner().getDp_terminZeitpunkt().getDatePicker().setDate(zwischen.getZeitpunkt().toLocalDate());
				StartGUI.frame.login.getLogin().getPlaner().getDp_terminZeitpunkt().getTimePicker().setTime(zwischen.getZeitpunkt().toLocalTime());
				String[] zwischenTeilnehmer;
				for (int i = 0; i < Controller.getAlleBenutzer().size(); i++) {
					zwischenTeilnehmer = new String[] {Controller.getAlleBenutzer().get(i).getName()};
					StartGUI.frame.login.getLogin().getPlaner().getModel_terminTeilnehmer().addRow(zwischenTeilnehmer);
					if (zwischen.getTeilnehmer().contains(new Benutzer(zwischenTeilnehmer[0]))) {
						StartGUI.frame.login.getLogin().getPlaner().getTable_terminTeilnehmer().setColumnSelectionInterval(0, 0);
						StartGUI.frame.login.getLogin().getPlaner().getTable_terminTeilnehmer().addRowSelectionInterval(i, i);
						StartGUI.frame.login.getLogin().getPlaner().getTable_terminTeilnehmer().requestFocus();
					}
				}
				if (zwischen.getHaeufigkeit() != -1) {
					StartGUI.frame.login.getLogin().getPlaner().getNumberChooser().setValue(zwischen.getHaeufigkeit());
				}else {
					StartGUI.frame.login.getLogin().getPlaner().getNumberChooser().setValue(1);
				}
				String[] zwischenErinnerungen;
				for (int j = 0; j < zwischen.getErinnerungsZeitpunkte().size(); j++) {
					zwischenErinnerungen = new String[] {zwischen.getErinnerungsZeitpunkte().get(j).getErinnerungsZeitpunkt().toLocalDate().toString(), zwischen.getErinnerungsZeitpunkte().get(j).getErinnerungsZeitpunkt().toLocalTime().toString()};
					StartGUI.frame.login.getLogin().getPlaner().getModel_terminErinnerung().addRow(zwischenErinnerungen);
				}
				switch (zwischen.getArtHaeufigkeit()) {
				case Termin.ALLE_TAGE:
					StartGUI.frame.login.getLogin().getPlaner().getChckbx_taeglich().setSelected(true);
					break;
				case Termin.ALLE_WOCHEN:
					StartGUI.frame.login.getLogin().getPlaner().getChckbx_woechentlich().setSelected(true);
					break;
				case Termin.ALLE_MONATE:
					StartGUI.frame.login.getLogin().getPlaner().getChckbx_monatlich().setSelected(true);
					break;
				case Termin.ALLE_JAHRE:
					StartGUI.frame.login.getLogin().getPlaner().getChckbx_jaehrlich().setSelected(true);
					break;
				default:
					StartGUI.frame.login.getLogin().getPlaner().getChckbx_einmalig().setSelected(true);
					break;
				}
			}
        });
        loeschen = new JMenuItem("L\u00f6schen");
        add(loeschen);
        loeschen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int id = (int) (StartGUI.frame.login.getLogin().getPlaner().getModel().getValueAt(getRow(),0));
				Controller.loescheTermin(id);
				StartGUI.frame.login.getLogin().getPlaner().aktualisieren();
			}
        });
		int id = (int) (StartGUI.frame.login.getLogin().getPlaner().getModel().getValueAt(getRow(),0));
		Termin zwischenT = Controller.getTermin(id);
		ImageIcon erledigtIcon;
        if (zwischenT.isErledigt() == false) {
			erledigtIcon = new ImageIcon(Images.checkbox_unclicked);
		}else {
			erledigtIcon = new ImageIcon(Images.checkbox_clicked);
		}
        erledigt = new JMenuItem("erledigt", erledigtIcon);
        erledigt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int id = (int) (StartGUI.frame.login.getLogin().getPlaner().getModel().getValueAt(getRow(),0));
				Termin t = Controller.getTermin(id);
				if (t.isErledigt() == false) {
					t.setErledigt(true);
					Controller.updateTermin(t);
				}else {
					t.setErledigt(false);
					Controller.updateTermin(t);
				}
				StartGUI.frame.login.getLogin().getPlaner().aktualisieren();
			}
        });
        add(erledigt);
    }

    //Getter und Setter
	public MouseEvent getEreignis() {
		return ereignis;
	}

	public void setEreignis(MouseEvent ereigniss) {
		this.ereignis = ereigniss;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}
}