package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;



import data.Images;
import logic.ImageReader;

public class StartGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	public static StartGUI frame;
	public static LoginStarter login;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ImageReader leser = new ImageReader();
					leser.readImages();
					frame = new StartGUI();
					login = new LoginStarter();
					login.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public StartGUI() {
		//Allgemein
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(((int)Toolkit.getDefaultToolkit().getScreenSize().getWidth()/2)-300, ((int)Toolkit.getDefaultToolkit().getScreenSize().getHeight()/2)-300, 600, 600);
		setTitle("Start - STB Terminplaner");
		setResizable(false);
		setIconImage(Images.logo_STB);
		setUndecorated(true);
		setBackground(new Color(0, 0, 0, 0));
		
		//Content-Pane
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setLayout(null);
		contentPane.setOpaque(false);
		setContentPane(contentPane);
			
		//Animation-Label
		JLabel startAnimation = new JLabel(Images.logo_STB_animiert);
		startAnimation.setBounds(0,0,550,550);
		contentPane.add(startAnimation);
		
		//Allgemein
		setVisible(true);
	}

	public LoginStarter getLoginStarter() {
		return login;
	}

}
