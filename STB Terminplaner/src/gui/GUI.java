package gui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.components.DateTimePicker;
import com.github.lgooddatepicker.components.TimePickerSettings;
import data.Images;
import guiEasterEgg.EasterEggGUI;
import logic.Controller;
import model.Benutzer;
import model.Erinnerung;
import model.Termin;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class GUI extends JFrame {

	private static final long serialVersionUID = 1L;
	
	// Elemente, welche global verf�gbar sein muessen (siehe Getters)
	private JPanel contentPane;
	private CardLayout karten;
	private JTextField txt_terminName;
	private JTextField txt_terminBeschreibungKurz;
	private JTextArea txt_terminBeschreibungLang;
	private DateTimePicker dp_terminZeitpunkt;
	private DefaultTableModel model_terminTeilnehmer;
	private JTable table_terminTeilnehmer;
	private DefaultTableModel model_terminErinnerung;
	private ButtonGroup buttonGroupRepeat;
	private JCheckBox chckbx_einmalig;
	private JCheckBox chckbx_taeglich;
	private JCheckBox chckbx_woechentlich;
	private JCheckBox chckbx_monatlich;
	private JCheckBox chckbx_jaehrlich;
	private JSpinner numberChooser;
	private JTable table;
	
	DefaultTableModel model;

	public GUI() {
		// Allgemeines
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2) - 600,
				((int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2) - 450, 1200, 900);
		setIconImage(Images.logo_STB);
		setResizable(true);
		setMinimumSize(new Dimension(900, 700));

		// Content-Pane
		karten = new CardLayout();
		contentPane = new JPanel(karten);

		// Main-Panel (beinhaltet die Termin-Tabelle)
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBackground(Color.DARK_GRAY);

		// Panel zum eintragen eines Termins
		GridBagLayout gbl1 = new GridBagLayout();
		GridBagConstraints gbc1 = new GridBagConstraints();
		gbc1.weightx = 0.5;
		gbc1.weighty = 0.5;
		gbc1.gridx = 0;
		gbc1.gridy = 0;
		gbc1.ipadx = 0;
		gbc1.ipady = 0;
		JPanel terminEintragenPanel = new JPanel(gbl1);
		terminEintragenPanel.setLayout(gbl1);
		terminEintragenPanel.setBackground(Color.DARK_GRAY);
		EmptyBorder borderEintragen = new EmptyBorder(0,100,0,0);
		terminEintragenPanel.setBorder(borderEintragen);
		
		// ----------------------------------------------------------------------------------------
		// Der Nutzer wird mit den folgenden Elementen informiert, was er im nebenstehenden Element 
		// auszuwaehlen hat.
		// ----------------------------------------------------------------------------------------
		
		JLabel lbl_terminName = new JLabel("Name:");
		lbl_terminName.setForeground(Color.white);
		lbl_terminName.setFont(new Font("Tahoma", Font.BOLD, 18));
		gbc1.anchor = GridBagConstraints.WEST;
		gbc1.ipadx = 0;
		gbc1.ipady = 0;
		terminEintragenPanel.add(lbl_terminName, gbc1);

		JLabel lbl_terminBeschreibungKurz = new JLabel("Beschreibung (Kurz):");
		lbl_terminBeschreibungKurz.setForeground(Color.white);
		lbl_terminBeschreibungKurz.setFont(new Font("Tahoma", Font.BOLD, 18));
		gbc1.gridy = 1;
		gbc1.ipadx = 0;
		gbc1.ipady = 0;
		terminEintragenPanel.add(lbl_terminBeschreibungKurz, gbc1);

		JLabel lbl_terminBeschreibungLang = new JLabel("Beschreibung (Lang):");
		lbl_terminBeschreibungLang.setForeground(Color.white);
		lbl_terminBeschreibungLang.setFont(new Font("Tahoma", Font.BOLD, 18));
		gbc1.gridy = 2;
		gbc1.ipadx = 0;
		gbc1.ipady = 0;
		terminEintragenPanel.add(lbl_terminBeschreibungLang, gbc1);

		JLabel lbl_terminZeitpunkt = new JLabel("Zeitpunkt:");
		lbl_terminZeitpunkt.setForeground(Color.white);
		lbl_terminZeitpunkt.setFont(new Font("Tahoma", Font.BOLD, 18));
		gbc1.gridy = 3;
		gbc1.ipadx = 0;
		gbc1.ipady = 0;
		terminEintragenPanel.add(lbl_terminZeitpunkt, gbc1);

		JLabel lbl_TerminTeilnehmer = new JLabel("Teilnehmer:");
		lbl_TerminTeilnehmer.setForeground(Color.white);
		lbl_TerminTeilnehmer.setFont(new Font("Tahoma", Font.BOLD, 18));
		gbc1.gridy = 4;
		gbc1.ipadx = 0;
		gbc1.ipady = 40;
		terminEintragenPanel.add(lbl_TerminTeilnehmer, gbc1);

		JLabel lbl_terminErinnerungen = new JLabel("Erinnerungen (Zeitpunkt):");
		lbl_terminErinnerungen.setForeground(Color.white);
		lbl_terminErinnerungen.setFont(new Font("Tahoma", Font.BOLD, 18));
		gbc1.gridy = 5;
		gbc1.ipadx = 0;
		gbc1.ipady = 0;
		terminEintragenPanel.add(lbl_terminErinnerungen, gbc1);

		// ------------------------------------------------------------
		// Die Daten eines Termins kann der Nutzer in folgenden Elementen 
		// auswaehlen oder niederschreiben
		// ------------------------------------------------------------
		
		// Name des Termins
		txt_terminName = new JTextField(30);
		txt_terminName.setFont(new Font("Tahoma", Font.BOLD, 16));
		txt_terminName.setBorder(null);
		gbc1.weightx = 0.5;
		gbc1.weighty = 0.5;
		gbc1.anchor = GridBagConstraints.WEST; 
		gbc1.gridx = 1;
		gbc1.gridy = 0;
		gbc1.ipadx = 300;
		gbc1.ipady = 15;
		terminEintragenPanel.add(txt_terminName, gbc1);

		//Kurze Beschreibung des Termins
		txt_terminBeschreibungKurz = new JTextField(30);
		txt_terminBeschreibungKurz.setFont(new Font("Tahoma", Font.BOLD, 16));
		txt_terminBeschreibungKurz.setBorder(null);
		gbc1.gridy = 1;
		gbc1.ipadx = 300;
		gbc1.ipady = 15;
		terminEintragenPanel.add(txt_terminBeschreibungKurz, gbc1);

		//Lange Beschreibung des Termins
		txt_terminBeschreibungLang = new JTextArea();
		txt_terminBeschreibungLang.setFont(new Font("Tahoma", Font.BOLD, 14));
		JScrollPane sp_terminBeschreibungLang = new JScrollPane(txt_terminBeschreibungLang);
		sp_terminBeschreibungLang.setBorder(null);
		gbc1.gridy = 2;
		gbc1.ipadx = 280;
		gbc1.ipady = 35;
		terminEintragenPanel.add(sp_terminBeschreibungLang, gbc1);

		// Datum und Zeit des Termins koennen vom Nutzer gewaehlt werden.
		DatePickerSettings dps_terminZeitpunkt = new DatePickerSettings();
		dps_terminZeitpunkt.setGapBeforeButtonPixels(5);
		TimePickerSettings dts_terminZeitpunkt = new TimePickerSettings();
		dts_terminZeitpunkt.setGapBeforeButtonPixels(5);
		dp_terminZeitpunkt = new DateTimePicker(dps_terminZeitpunkt, dts_terminZeitpunkt);
		dp_terminZeitpunkt.setBorder(null);
		dp_terminZeitpunkt.setBackground(Color.DARK_GRAY);
		dp_terminZeitpunkt.getTimePicker().setBackground(Color.DARK_GRAY);
		dp_terminZeitpunkt.getDatePicker().setBackground(Color.DARK_GRAY);
		dp_terminZeitpunkt.setBackground(Color.DARK_GRAY);
		gbc1.gridy = 3;
		gbc1.ipadx = 32;
		gbc1.ipady = 0;
		terminEintragenPanel.add(dp_terminZeitpunkt, gbc1);

		model_terminTeilnehmer = new DefaultTableModel() {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				// keine Zelle kann bearbeitet werden
				return false;
			}
		};
		table_terminTeilnehmer = new JTable(model_terminTeilnehmer);
		table_terminTeilnehmer.setFont(table_terminTeilnehmer.getFont().deriveFont((float) 14));
		table_terminTeilnehmer.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 18));
		table_terminTeilnehmer.setEnabled(true);
		table_terminTeilnehmer.setBorder(null);
		table_terminTeilnehmer.setCellSelectionEnabled(true);
		model_terminTeilnehmer.addColumn("Teilnehmer");
		table_terminTeilnehmer.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane sp_terminTeilnehmer = new JScrollPane(table_terminTeilnehmer);
		sp_terminTeilnehmer.setBorder(null);
		gbc1.gridy = 4;
		gbc1.ipadx = 280;
		gbc1.ipady = 100;
		terminEintragenPanel.add(sp_terminTeilnehmer, gbc1);

		// Erinnerungs-Panel
		JPanel terminErinnerungPanel = new JPanel(new FlowLayout());
		terminErinnerungPanel.setBackground(Color.DARK_GRAY);
		gbc1.gridy = 5;
		gbc1.ipadx = 0;
		gbc1.ipady = 0;
		terminEintragenPanel.add(terminErinnerungPanel, gbc1);

		// Datum und Zeit der Erinnerung koennen mit dem DateTimePicker vom Nutzer ausgewaehlt werden
		DatePickerSettings dps_terminErinnerungZeitpunkt = new DatePickerSettings();
		dps_terminErinnerungZeitpunkt.setGapBeforeButtonPixels(5);
		TimePickerSettings dts_terminErinnerungZeitpunkt = new TimePickerSettings();
		dts_terminErinnerungZeitpunkt.setGapBeforeButtonPixels(5);
		DateTimePicker dp_terminErinnerungZeitpunkt = new DateTimePicker(dps_terminErinnerungZeitpunkt,
				dts_terminErinnerungZeitpunkt);
		dp_terminErinnerungZeitpunkt.setBorder(null);
		dp_terminErinnerungZeitpunkt.setBackground(Color.DARK_GRAY);
		dp_terminErinnerungZeitpunkt.getTimePicker().setBackground(Color.DARK_GRAY);
		dp_terminErinnerungZeitpunkt.getDatePicker().setBackground(Color.DARK_GRAY);
		dp_terminErinnerungZeitpunkt.setBackground(Color.DARK_GRAY);
		terminErinnerungPanel.add(dp_terminErinnerungZeitpunkt, gbc1);

		// Erinnerungs-Anzeige (fuer Nutzer und zur Zwischenspeicherung)
		model_terminErinnerung = new DefaultTableModel() {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				// keine Zelle kann bearbeitet werden
				return false;
			}
		};
		JTable table_terminErinnerung = new JTable(model_terminErinnerung);
		table_terminErinnerung.setFont(table_terminErinnerung.getFont().deriveFont((float) 14));
		table_terminErinnerung.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 18));
		table_terminErinnerung.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_terminErinnerung.setEnabled(true);
		table_terminErinnerung.setBorder(null);
		model_terminErinnerung.addColumn("Datum");
		model_terminErinnerung.addColumn("Zeit");
		table_terminErinnerung.getColumnModel().getColumn(0).setMinWidth(100);
		table_terminErinnerung.getColumnModel().getColumn(1).setMinWidth(100);
		table_terminErinnerung.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane sp_terminErinnerung = new JScrollPane(table_terminErinnerung);
		sp_terminErinnerung.setBorder(null);
		gbc1.gridy = 5;
		gbc1.gridx = 2;
		gbc1.ipadx = 280;
		gbc1.ipady = 100;
		terminEintragenPanel.add(sp_terminErinnerung, gbc1);

		// Erinnerung Hinzufuegen Button
		List<Erinnerung> ausgewaehltZeiten = new ArrayList<Erinnerung>();
		JButton btn_terminErinnerungsEintragen = new JButton("  +");
		// Das ausgewaehlte Datum sowie die Zeit werden in 
		// Strings umgewandelt und anschlie�end in einer Tabelle angezeigt (zur Zwischenspeicherung).
		btn_terminErinnerungsEintragen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				String[] erinnerung = new String[2];
				if (dp_terminErinnerungZeitpunkt.getDatePicker().getDate() != null) {
					LocalDate date_Erinnerung = dp_terminErinnerungZeitpunkt.getDatePicker().getDate();
					DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd.LLLL.yyyy");
					String date_String = date_Erinnerung.format(formatterDate);
					erinnerung[0] = date_String;
					ausgewaehltZeiten.add(new Erinnerung(dp_terminErinnerungZeitpunkt.getDateTimePermissive(), false));
				}
				if (dp_terminErinnerungZeitpunkt.getTimePicker().getTime() != null) {
					LocalTime time_Erinnerung = dp_terminErinnerungZeitpunkt.getTimePicker().getTime();
					DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm");
					String time_String = time_Erinnerung.format(formatterTime);
					erinnerung[1] = time_String;
				}
				if (erinnerung[0] != null && erinnerung[1] != null) {
					model_terminErinnerung.addRow(erinnerung);
				}
			}
		});
		// Formatierung
		btn_terminErinnerungsEintragen.setFocusable(false);
		btn_terminErinnerungsEintragen.setBorder(null);
		btn_terminErinnerungsEintragen.setContentAreaFilled(false);
		btn_terminErinnerungsEintragen.setForeground(Color.white);
		btn_terminErinnerungsEintragen.setFont(new Font("Tahoma", Font.BOLD, 18));
		// Button hinzufuegen
		terminErinnerungPanel.add(btn_terminErinnerungsEintragen, gbc1);
		
		// Erinnerung entfernen Button
		JButton btn_terminErinnerungsEntfernen = new JButton("  -");
		// Ausgewaehlte Zeiten werden aus der Tabelle geloescht
		btn_terminErinnerungsEntfernen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				if (table_terminErinnerung.getSelectedRow() >= 0) {
					model_terminErinnerung.removeRow(table_terminErinnerung.getSelectedRow());
				}
			}
		});
		// Formatierung
		btn_terminErinnerungsEntfernen.setFocusable(false);
		btn_terminErinnerungsEntfernen.setBorder(null);
		btn_terminErinnerungsEntfernen.setContentAreaFilled(false);
		btn_terminErinnerungsEntfernen.setForeground(Color.white);
		btn_terminErinnerungsEntfernen.setFont(new Font("Tahoma", Font.BOLD, 18));
		// Button hinzufuegen
		terminErinnerungPanel.add(btn_terminErinnerungsEntfernen, gbc1);
		
		
		// Der Nutzer kann auswaehlen, ob bzw. wie oft sich sein Termin wiederholt
		JPanel contentTraegerPanel = new JPanel(new BorderLayout());
		contentTraegerPanel.setBackground(Color.DARK_GRAY);
		JPanel oberesAuswahlPanel = new JPanel(new FlowLayout());
		oberesAuswahlPanel.setBackground(Color.DARK_GRAY);
		JLabel beschriftung = new JLabel("Alle");
		beschriftung.setFont(new Font("Tahoma", Font.BOLD, 18));
		beschriftung.setForeground(Color.white);
		beschriftung.setBackground(Color.DARK_GRAY);
        SpinnerNumberModel numberModel = new SpinnerNumberModel(
                new Integer(1), // value
                new Integer(1), // min
                new Integer(99), // max
                new Integer(1) // step
                );
        numberChooser = new JSpinner(numberModel);
		contentTraegerPanel.add(oberesAuswahlPanel);
		JPanel terminRepeat = new JPanel(new GridLayout(2,2));
		terminRepeat.setBackground(Color.DARK_GRAY);
		buttonGroupRepeat = new ButtonGroup();
		chckbx_einmalig = new JCheckBox("einmalig");
		buttonGroupRepeat.add(chckbx_einmalig);
		chckbx_einmalig.setBackground(Color.DARK_GRAY);
		chckbx_einmalig.setForeground(Color.white);
		chckbx_einmalig.setFocusable(false);
		oberesAuswahlPanel.add(chckbx_einmalig);
		oberesAuswahlPanel.add(beschriftung);
		oberesAuswahlPanel.add(numberChooser);
		chckbx_taeglich = new JCheckBox("Tage");
		buttonGroupRepeat.add(chckbx_taeglich);
		chckbx_taeglich.setBackground(Color.DARK_GRAY);
		chckbx_taeglich.setForeground(Color.white);
		chckbx_taeglich.setFocusable(false);
		terminRepeat.add(chckbx_taeglich);
		chckbx_woechentlich = new JCheckBox("Wochen");
		buttonGroupRepeat.add(chckbx_woechentlich);
		chckbx_woechentlich.setBackground(Color.DARK_GRAY);
		chckbx_woechentlich.setForeground(Color.white);
		chckbx_woechentlich.setFocusable(false);
		terminRepeat.add(chckbx_woechentlich);
		chckbx_monatlich = new JCheckBox("Monate");
		buttonGroupRepeat.add(chckbx_monatlich);
		chckbx_monatlich.setBackground(Color.DARK_GRAY);
		chckbx_monatlich.setForeground(Color.white);
		chckbx_monatlich.setFocusable(false);
		terminRepeat.add(chckbx_monatlich);
		chckbx_jaehrlich = new JCheckBox("Jahre");
		buttonGroupRepeat.add(chckbx_jaehrlich);
		chckbx_jaehrlich.setBackground(Color.DARK_GRAY);
		chckbx_jaehrlich.setForeground(Color.white);
		chckbx_jaehrlich.setFocusable(false);
		terminRepeat.add(chckbx_jaehrlich);
		gbc1.gridy = 3;
		gbc1.gridx = 2;
		gbc1.ipadx = 0;
		gbc1.ipady = 0;
		contentTraegerPanel.add(terminRepeat, BorderLayout.CENTER);
		contentTraegerPanel.add(oberesAuswahlPanel, BorderLayout.NORTH);
		terminEintragenPanel.add(contentTraegerPanel, gbc1);
		
		// Fuell-Panel
		JPanel fuellTerminEintragenPanel = new JPanel(new FlowLayout());
		fuellTerminEintragenPanel.setBackground(Color.DARK_GRAY);
		gbc1.ipadx = 0;
		gbc1.ipady = 80;
		gbc1.gridx = 1;
		gbc1.gridy = 6;
		terminEintragenPanel.add(fuellTerminEintragenPanel, gbc1);

		// Button zum Eintragen eines Termins
		JButton btn_terminSpeichern = new JButton();
		btn_terminSpeichern.setIcon(new ImageIcon(Images.eintragen_button));
		btn_terminSpeichern.setBorder(null);
		btn_terminSpeichern.setFocusable(false);
		btn_terminSpeichern.setContentAreaFilled(false);
		btn_terminSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				if (!txt_terminName.getText().equals("") && !txt_terminBeschreibungKurz.getText().equals("") && !txt_terminBeschreibungLang.getText().equals("") && dp_terminZeitpunkt.getDateTimePermissive()!= null &&  table_terminTeilnehmer.getSelectedRowCount() != 0) {
					List<Benutzer> ausgewaehltTeilnehmer = new ArrayList<Benutzer>();
					int[] row = table_terminTeilnehmer.getSelectedRows(); 
					for (int i = 0; i < table_terminTeilnehmer.getSelectedRowCount(); i++) {
						ausgewaehltTeilnehmer.add(new Benutzer((String)table_terminTeilnehmer.getValueAt(row[i], 0)));
					}
					int artHauefigkeit;
					if (chckbx_einmalig.isSelected() == true) {
						Controller.terminEintragen(txt_terminName.getText(), txt_terminBeschreibungKurz.getText(), txt_terminBeschreibungLang.getText(), dp_terminZeitpunkt.getDateTimePermissive(), ausgewaehltTeilnehmer, ausgewaehltZeiten, false);
					}else {
						if (chckbx_taeglich.isSelected()) {
							artHauefigkeit = 0;
						}else if (chckbx_woechentlich.isSelected()) {
							artHauefigkeit = 1;
						}else if (chckbx_monatlich.isSelected()) {
							artHauefigkeit = 2;
						}else {
							artHauefigkeit = 3;
						}
						Controller.terminEintragen(txt_terminName.getText(), txt_terminBeschreibungKurz.getText(), txt_terminBeschreibungLang.getText(), dp_terminZeitpunkt.getDateTimePermissive(), ausgewaehltTeilnehmer, ausgewaehltZeiten, (int)numberChooser.getValue(), artHauefigkeit, false);
					}
					aktualisieren();
					karten.show(contentPane, "main");
				}else {
			           JOptionPane.showMessageDialog(null, "F\u00fcllen Sie bitte alle Felder aus!");
				}
			}
		});
		fuellTerminEintragenPanel.add(btn_terminSpeichern);
		
		// ------------------------------------------------------------------
		// Einstellungs Panel, stellt dem Nutzer Einstellungen zur Verfuegung
		// ------------------------------------------------------------------
		GridBagLayout gblS = new GridBagLayout();
		GridBagConstraints gbcS = new GridBagConstraints();
		JPanel settings = new JPanel();
		EmptyBorder borderS = new EmptyBorder(0,80,0,0);
		settings.setBorder(borderS);
		settings.setLayout(gblS);
		settings.setBackground(Color.DARK_GRAY);
		
		JLabel lbl_infoRepeatTermine= new JLabel("Einstellungen");
		lbl_infoRepeatTermine.setForeground(Color.white);
		lbl_infoRepeatTermine.setBackground(Color.DARK_GRAY);
		lbl_infoRepeatTermine.setFont(new Font("Tahoma", Font.BOLD, 22));
		gbcS.weightx = 0.5;
		gbcS.weighty = 0.5;
		gbcS.anchor = GridBagConstraints.CENTER; 
		gbcS.gridx = 1;
		gbcS.gridy = 0;
		gbcS.ipadx = 0;
		gbcS.ipady = 0;
		settings.add(lbl_infoRepeatTermine, gbcS);
		
		JLabel lbl_infoAnzahlWiederkehrenderTermine= new JLabel("Anzahl anzuzeigender wiederkehrender Termine: ");
		lbl_infoAnzahlWiederkehrenderTermine.setForeground(Color.white);
		lbl_infoAnzahlWiederkehrenderTermine.setBackground(Color.DARK_GRAY);
		lbl_infoAnzahlWiederkehrenderTermine.setFont(new Font("Tahoma", Font.BOLD, 16));
		gbcS.anchor = GridBagConstraints.WEST; 
		gbcS.gridx = 0;
		gbcS.gridy = 1;
		gbcS.ipadx = 0;
		gbcS.ipady = 0;
		settings.add(lbl_infoAnzahlWiederkehrenderTermine, gbcS);
		
        SpinnerNumberModel numberModelS = new SpinnerNumberModel(
                new Integer(1), // value
                new Integer(0), // min
                new Integer(5), // max
                new Integer(1) // step
                );
        JSpinner numberChooserS = new JSpinner(numberModelS);
		gbcS.anchor = GridBagConstraints.WEST; 
		gbcS.gridx = 1;
		gbcS.gridy = 1;
		gbcS.ipadx = 0;
		gbcS.ipady = 0;
		settings.add(numberChooserS, gbcS);
		
		JLabel lbl_infoZeigeEreldigteTermine= new JLabel("Erledigte Termine anzeigen: ");
		lbl_infoZeigeEreldigteTermine.setForeground(Color.white);
		lbl_infoZeigeEreldigteTermine.setBackground(Color.DARK_GRAY);
		lbl_infoZeigeEreldigteTermine.setFont(new Font("Tahoma", Font.BOLD, 16));
		gbcS.anchor = GridBagConstraints.WEST; 
		gbcS.gridx = 0;
		gbcS.gridy = 2;
		gbcS.ipadx = 0;
		gbcS.ipady = 0;
		settings.add(lbl_infoZeigeEreldigteTermine, gbcS);
		
		JPanel checkBoxPanel = new JPanel();
		checkBoxPanel.setBackground(Color.DARK_GRAY);
		gbcS.anchor = GridBagConstraints.WEST; 
		gbcS.gridx = 1;
		gbcS.gridy = 2;
		gbcS.ipadx = 0;
		gbcS.ipady = 0;
		settings.add(checkBoxPanel, gbcS);
		
		ButtonGroup buttonGroupS = new ButtonGroup();
		
		JCheckBox chckbx_An = new JCheckBox("An");
		chckbx_An.setSelected(true);
		buttonGroupS.add(chckbx_An);
		chckbx_An.setBackground(Color.DARK_GRAY);
		chckbx_An.setForeground(Color.white);
		chckbx_An.setFocusable(false);
		checkBoxPanel.add(chckbx_An);
		
		JCheckBox chckbx_Aus = new JCheckBox("Aus");
		buttonGroupS.add(chckbx_Aus);
		chckbx_Aus.setBackground(Color.DARK_GRAY);
		chckbx_Aus.setForeground(Color.white);
		chckbx_Aus.setFocusable(false);
		checkBoxPanel.add(chckbx_Aus);
		
		// Fuell-Elemente
		JPanel rechtsFueller = new JPanel();
		rechtsFueller.setBackground(Color.DARK_GRAY);
		gbcS.gridx = 2;
		gbcS.gridy = 0;
		gbcS.ipadx = 450;
		gbcS.ipady = 0;
		settings.add(rechtsFueller, gbcS);
		
		JPanel untenFuellerUndButtonPanel = new JPanel(new FlowLayout());
		untenFuellerUndButtonPanel.setBackground(Color.DARK_GRAY);
		gbcS.anchor = GridBagConstraints.CENTER; 
		gbcS.gridx = 1;
		gbcS.gridy = 3;
		gbcS.ipadx = 0;
		gbcS.ipady = 300;
		settings.add(untenFuellerUndButtonPanel, gbcS);
		
		// Speichert die Einstellungen bzw. setzt sie um
		JButton btn_settingsSpeichern = new JButton();
		btn_settingsSpeichern.setIcon(new ImageIcon(Images.speichern_button));
		btn_settingsSpeichern.setBorder(null);
		btn_settingsSpeichern.setFocusable(false);
		btn_settingsSpeichern.setContentAreaFilled(false);
		btn_settingsSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				if (chckbx_An.isSelected()) {
					Controller.setErledigteTermineAnzeigen(true);
				}else {
					Controller.setErledigteTermineAnzeigen(false);
				}
				Controller.setAnzahlWiederkehrendeTermine((int)numberChooserS.getValue());
				aktualisieren();
				karten.show(contentPane, "main");
			}
		});
		untenFuellerUndButtonPanel.add(btn_settingsSpeichern);
		
		// ---------------------------------------------
		// Hilfe Panel, erklaert das Programm dem Nutzer
		// ---------------------------------------------
		GridLayout grid1 = new GridLayout(2,1);
		EmptyBorder border1 = new EmptyBorder(0,100,0,0);
		JPanel hilfePanel = new JPanel(grid1);
		hilfePanel.setBackground(Color.DARK_GRAY);
		JLabel helpLabel = new JLabel(
				"<html><body><center><h1 style=\"color:#FFFFFF\">Help</h1></center><div align=\"left\"><FONT SIZE=\"5\"><span style=\"color:#FFFFFF\"><br>Dieses Programm dient als Termin-Verwaltung. "
				+ "Dabei k\u00f6nnen Termine erstellt, bearbeitet und gel\u00f6scht werden."
				+ "<br>Durch einen Rechtsklick auf einen Termin wird ein entsprechendes Men\u00fc angezeigt, "
				+ "<br>welches die erw\u00e4hnten Optionen bereitstellt."
				+ "<br>Au\u00dferdem kann die Termin-Liste exportiert und auch wieder importiert werden,"
				+ "<br>dazu verwenden Sie einfach die Men\u00fcleiste oben rechts in der Software."
				+ "<br>Diese stellt Ihnen zudem weitere M\u00f6glichkeiten zur Verf\u00fcgung,"
				+ "<br>wie das Ausloggen, Aktualisieren der Liste oder die M\u00f6glichkeit,"
				+ "<br>zum Einstellungs-Screen zu gelangen. Auf diesem lassen sich Einstellungen zur"
				+ "<br>zum individualisieren der Termin-Tabelle treffen."
				+ "<br>Wenn Sie Fragen zur Bedienung der Software haben, wenden Sie sich an folgende E-Mail-Adresse."
				+ "<br><br>simplythebest\u0040straightforward.de"
				+ "<br><br><span style=\"color:#FF0000\">Warnung: Zehnbusch nicht \u00e4rgern</span></div></font></body></html>");
		hilfePanel.add(helpLabel);
		helpLabel.setBorder(border1);
		JPanel panelFuerGeheimeButtons = new JPanel(new FlowLayout());
		panelFuerGeheimeButtons.setBackground(Color.DARK_GRAY);
		JButton benutzer_loeschen = new JButton();
		benutzer_loeschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				karten.show(contentPane, "userDEL");
			}
		});
		benutzer_loeschen.setPreferredSize(new Dimension(200,80));
		benutzer_loeschen.setFocusable(false);
		benutzer_loeschen.setBorder(null);
		benutzer_loeschen.setContentAreaFilled(false);
		panelFuerGeheimeButtons.add(benutzer_loeschen);
		JPanel fuellerZwischenGeheimButtons = new JPanel();
		fuellerZwischenGeheimButtons.setBackground(Color.DARK_GRAY);
		fuellerZwischenGeheimButtons.setPreferredSize(new Dimension(300,10));
		panelFuerGeheimeButtons.add(fuellerZwischenGeheimButtons);
		JButton eastereggStarten = new JButton();
		eastereggStarten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				EasterEggGUI easterEgg = new EasterEggGUI();
				easterEgg.erstelleBuffer();
				easterEgg.start();
			}
		});
		eastereggStarten.setPreferredSize(new Dimension(200,80));
		eastereggStarten.setFocusable(false);
		eastereggStarten.setBorder(null);
		eastereggStarten.setContentAreaFilled(false);
		panelFuerGeheimeButtons.add(eastereggStarten);
		hilfePanel.add(panelFuerGeheimeButtons);
		
		// -----------------------------------
		// Benutzer loeschen Panel (easteregg)
		// -----------------------------------
		JPanel benutzer_loeschenPanel = new JPanel(new FlowLayout());
		EmptyBorder zentrierer = new EmptyBorder(200,0,0,0);
		benutzer_loeschenPanel.setBorder(zentrierer);
		benutzer_loeschenPanel.setBackground(Color.DARK_GRAY);
		JPanel content_benutzerLoeschen = new JPanel();
		content_benutzerLoeschen.setBackground(Color.DARK_GRAY);
		content_benutzerLoeschen.setPreferredSize(new Dimension(400,500));
		benutzer_loeschenPanel.add(content_benutzerLoeschen);
		// Benutzer Tabelle zum auswaehlen der zu loeschenden Nutzer
		DefaultTableModel model_benutzerLoeschen = new DefaultTableModel() {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				// keine Zelle kann bearbeitet werden
				return false;
			}
		};
		JTable table_benutzerLoeschen = new JTable(model_benutzerLoeschen);
		table_benutzerLoeschen.setFont(table_benutzerLoeschen.getFont().deriveFont((float) 14));
		table_benutzerLoeschen.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 18));
		table_benutzerLoeschen.setPreferredScrollableViewportSize(new Dimension(300, 330));
		table_benutzerLoeschen.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_benutzerLoeschen.setEnabled(true);
		table_benutzerLoeschen.setBorder(null);
		model_benutzerLoeschen.addColumn("Benutzer");
		table_benutzerLoeschen.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		List<Benutzer> zwischenNutzerListeBenutzerLoeschen = Controller.getAlleBenutzer();
		for (int i = 0; i < Controller.getAlleBenutzer().size(); i++) {
			Object[] zwischenNutzer = {zwischenNutzerListeBenutzerLoeschen.get(i).getName()};
			model_benutzerLoeschen.addRow(zwischenNutzer);
		}
		// Um runterscrollen zu koennen, wir ein die gesammte Tabelle auf ein ScrollPane gepackt
		JScrollPane scrollPaneBenutzerLoeschen = new JScrollPane(table_benutzerLoeschen);
		content_benutzerLoeschen.add(scrollPaneBenutzerLoeschen);
		// Button zum Nutzer loeschen, loescht den Benutzer und aktualisiert die Tabelle
		JButton loescheBenutzerButton = new JButton("Nutzer l\u00f6schen");
		loescheBenutzerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				String ausgewaehlt = (String)table_benutzerLoeschen.getValueAt(table_benutzerLoeschen.getSelectedRow(), 0);
				if (!ausgewaehlt.equals(Controller.getEingeloggterBenutzer().getName())) {
					Controller.benutzerLoeschen((String)table_benutzerLoeschen.getValueAt(table_benutzerLoeschen.getSelectedRow(), 0));
					model_benutzerLoeschen.setRowCount(0);
					List<Benutzer> zwischenNutzerListeBenutzerLoeschen = Controller.getAlleBenutzer();
					for (int i = 0; i < Controller.getAlleBenutzer().size(); i++) {
						Object[] zwischenNutzer = {zwischenNutzerListeBenutzerLoeschen.get(i).getName()};
						model_benutzerLoeschen.addRow(zwischenNutzer);
					}
				}else {
			        JOptionPane.showMessageDialog(null, "Sie k\u00f6nnen sich nicht selbst l\u00f6schen");
				}
			}
		});
		//loescheBenutzerButton.setContentAreaFilled(false);
		loescheBenutzerButton.setFocusable(false);
		loescheBenutzerButton.setBackground(Color.DARK_GRAY);
		loescheBenutzerButton.setForeground(Color.WHITE);
		content_benutzerLoeschen.add(loescheBenutzerButton);
		
		// Menue Leiste erstellen
		JMenuBar menuBar = new JMenuBar();
		JMenu menuOptionen = new JMenu("Optionen");
		JMenu menuHelp = new JMenu("Help");

		// -----------------------------
		// Panel feur die Termin Tabelle
		// -----------------------------
		JPanel tablePanel = new JPanel(new BorderLayout());
		// Modell welches alle Termine des eingeloggten Nutzers beinhaltet
		model = new DefaultTableModel() {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				// keine Zelle kann bearbeitet werden
				return false;
			}
		};
		// Haupt-Tabelle - zeigt alle Termine
		table = new JTable(model);
        // Der TableRowSorter wird die Daten des Models sortieren
		// bzw. gibt es dem Nutzer die Moeglichkeit dies selbst zu tun
        TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>();
        table.setRowSorter(sorter);
        sorter.setModel(model);
        // Bei rechtsklick auf einen Termin, wird ein Menue angezeigt.
		table.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				Point p = e.getPoint();
				int row = table.rowAtPoint(p);
				int column = table.columnAtPoint(p);
				p.setLocation(row, column);
				if (e.isPopupTrigger())
					zeigeMenu(e, p);
			}

			private void zeigeMenu(MouseEvent e, Point p) {
				TerminMenu menu = new TerminMenu(e, p);
				menu.show(e.getComponent(), e.getX(), e.getY());
			}
		});
		// Formatierung und einstellen des Tabels und des Daten-Models
		table.setFont(table.getFont().deriveFont((float) 14));
		table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 18));
		table.setEnabled(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// Spalten-Ueberschriften
		model.addColumn("ID");
		model.addColumn("Zeit");
		model.addColumn("Termin");
		model.addColumn("Kurzbeschreibung");
		model.addColumn("Erledigt");
		// Bei schweben der Maus ueber einem Termin in der Kurzbeschreibungs-Spalte 
		// wird die lange Beschreibung des Termins als Tooltip angezeigt
		table.getColumnModel().getColumn(3).setCellRenderer(new ToolTipRender());
		table.getColumnModel().getColumn(4).setCellRenderer(new StatusColumnCellRenderer());
		// Spalten Formatieren
		table.getColumnModel().getColumn(0).setPreferredWidth(0);;
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(1).setMinWidth(300);
		table.getColumnModel().getColumn(2).setMinWidth(300);
		table.getColumnModel().getColumn(3).setMinWidth(300);
		table.getColumnModel().getColumn(4).setMinWidth(250);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		// Zeilen Formatieren
		table.setRowHeight(30);
		table.setFont(table.getFont().deriveFont(16.0f));
		// Um runterscrollen zu koennen, wir ein die gesammte Tabelle auf ein ScrollPane gepackt
		JScrollPane scrollPane = new JScrollPane(table);
		// Rand
		EmptyBorder borderTable = new EmptyBorder(30, 30, 30, 30);
		mainPanel.setBorder(borderTable);
		// Zusammenfuegen der Elemente
		tablePanel.add(scrollPane, BorderLayout.CENTER);
		mainPanel.add(tablePanel, BorderLayout.CENTER);

		// Button welcher zum Termin eintragen zust�ndigen Panel weiterleitet
		// und dieses entsprechend leert.
		JPanel buttonPanel = new JPanel(new FlowLayout());
		buttonPanel.setBackground(Color.DARK_GRAY);
		JButton btn_terminEintragen = new JButton();
		btn_terminEintragen.setIcon(new ImageIcon(Images.terminEintragen_button));
		btn_terminEintragen.setBorder(null);
		btn_terminEintragen.setFocusable(false);
		btn_terminEintragen.setContentAreaFilled(false);
		btn_terminEintragen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				txt_terminName.setText("");
				txt_terminBeschreibungLang.setText("");
				txt_terminBeschreibungKurz.setText("");
				dp_terminZeitpunkt.getDatePicker().setDate(null);
				dp_terminZeitpunkt.getTimePicker().setTime(null);
				dp_terminErinnerungZeitpunkt.getDatePicker().setDate(null);
				dp_terminErinnerungZeitpunkt.getTimePicker().setTime(null);
				buttonGroupRepeat.clearSelection();
				dp_terminErinnerungZeitpunkt.getDatePicker().setDate(null);
				dp_terminErinnerungZeitpunkt.getTimePicker().setTime(null);
				model_terminErinnerung.setRowCount(0);
				model_terminTeilnehmer.setRowCount(0);
				List<Benutzer> zwischenNutzerListe = Controller.getAlleBenutzer();
				for (int i = 0; i < Controller.getAlleBenutzer().size(); i++) {
					Object[] zwischenNutzer = {zwischenNutzerListe.get(i).getName()};
					model_terminTeilnehmer.addRow(zwischenNutzer);
				}
				karten.show(contentPane, "termin");
			}
		});
		buttonPanel.add(btn_terminEintragen);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);

		// Card-Layout einstellen
		contentPane.add(mainPanel, "main");
		contentPane.add(terminEintragenPanel, "termin");
		contentPane.add(hilfePanel, "hilfe");
		contentPane.add(benutzer_loeschenPanel, "userDEL");
		contentPane.add(settings, "settings");
		karten.show(contentPane, "main"); // Screen bei Start

		// Menue Leiste einstellen
		JMenuItem hauptMenue = new JMenuItem("Hauptmen\u00fc");
		hauptMenue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				karten.show(contentPane, "main");
			}
		});
		// Zum aktualisieren der Termin-Tabelle
		JMenuItem aktualisieren = new JMenuItem("Aktualisieren");
		aktualisieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				aktualisieren();
			}
		});
		JMenuItem logout = new JMenuItem("Ausloggen");
		logout.addActionListener(new ActionListener() {
			@SuppressWarnings({ "deprecation", "static-access" })
			public void actionPerformed(ActionEvent ev) {
				Login.checker.stop();
				Controller.logout();
				getGUI().setVisible(false);
				StartGUI.frame.login.getLogin().setVisible(true);
			}
		});
		//Datenbank mit allen Bestandteilen/Daten einlesen
		JMenuItem importieren = new JMenuItem("Importieren");
		importieren.addActionListener(new ActionListener() {
			@SuppressWarnings({ "deprecation", "static-access" })
			public void actionPerformed(ActionEvent ev) {
				JFileChooser chooser = new JFileChooser();        //File-Chooser wird erstellt mit dem eine vorhandene Datenbank ausgewaehlt werden kann
		        chooser.setCurrentDirectory(new File(System.getProperty("user.home")+ System.getProperty("file.separator")+ "Desktop"));	//setzt das Startverzeichniss auf den Desktop
		        chooser.showOpenDialog(null); // Dialog zum Oeffnen von Dateien anzeigen
		        File f = chooser.getSelectedFile();	// ausgewaehlte Datei wird gespeichert
		        if (f != null) {
					if (chooser.getTypeDescription(f).equals("STBT-Datei")) {    // Es wird abgefragt, ob die gewaehlte Datei eine stbt-Datei ist, wenn dies der Fall ist wird der
						boolean erfolgreich = Controller.importieren(f);
						if (erfolgreich) {
					        JOptionPane.showMessageDialog(null, "Datei erfolgreich eingelesen!"); // Name der Datei ausgegeben/Bestaetigung
							Controller.logout();
							getGUI().setVisible(false);
							Login.checker.stop();
							StartGUI.frame.login.getLogin().setVisible(true);
						}else {
					        JOptionPane.showMessageDialog(null, "Beim Einlesen ist ein Fehler aufgetreten!"); // Einlesen ohne Erfolg
						}
					}
			        else {
			        	JOptionPane.showMessageDialog(null, "Dateityp-stbt erforderlich!"); // Wenn nicht, dann wird eine Fehlermeldung ausgegeben
					}
				}
			}
		});
		JMenuItem exportieren = new JMenuItem("Exportieren");
		exportieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				JFileChooser chooser = new JFileChooser();        //File-Chooser wird erstellt mit dem eine vorhandene Datenbank ausgewaehlt werden kann
		        chooser.setCurrentDirectory(new File(System.getProperty("user.home")+ System.getProperty("file.separator")+ "Desktop"));	//setzt das Startverzeichniss auf den Desktop
		        chooser.showSaveDialog(null); // Dialog zum Oeffnen von Dateien anzeigen
				File f = new File(chooser.getSelectedFile().getAbsolutePath() + ".stbt");    // ausgewaehlte Datei wird gespeichert
						boolean erfolgreich = Controller.exportieren(f);
						if (erfolgreich) {
					        JOptionPane.showMessageDialog(null, "Daten erfolgreich gespeichert!"); // Name der Datei ausgegeben/Bestaetigung
						} else {
					        JOptionPane.showMessageDialog(null, "Beim Speichern ist ein Fehler aufgetreten!"); // Einlesen ohne Erfolg
						}
			}
		});
		JMenuItem einstellugen = new JMenuItem("Einstellungen");
		einstellugen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				karten.show(contentPane, "settings");
			}
		});
		// Informationen ueber das Program werden angezeigt
		JMenuItem hilfe = new JMenuItem("Help");
		hilfe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				karten.show(contentPane, "hilfe");
			}
		});
		//Hinzufuegen von Menueeintraegen in das Dateimenue
		menuOptionen.add(hauptMenue);
		menuOptionen.add(aktualisieren);
		menuOptionen.add(importieren);
		menuOptionen.add(exportieren);
		menuOptionen.add(einstellugen);
		menuOptionen.add(logout);
		menuHelp.add(hilfe);
		menuBar.add(menuOptionen);
		menuBar.add(menuHelp);
		setJMenuBar(menuBar);

		//Termin Tabelle befuellen
		aktualisieren();
		
		// Allgemeines
		setContentPane(contentPane);
		setVisible(true);
	}

	/*
	 * Methode zum aktualisieren der Termin-Tabelle
	 *  -	Der naechst anliegende Termin wird makiert.
	 *  - 	Der Terminzeitpunkt wird entsprechend der deutschen Zeitdarstellung formatiert.
	 */
	public void aktualisieren() {
		this.getModel().setRowCount(0);
		List<Termin> zwischenT = Controller.getTermine();				
		Object[] zwischenTDaten;
		for (int i = 0; i < Controller.getTermine().size(); i++) {
			DateTimeFormatter formatierer = DateTimeFormatter.ofPattern("dd.MM.yyyy 'um' HH:mm'Uhr'");
			String zeitpunkt = formatierer.format(zwischenT.get(i).getZeitpunkt());
			if (zwischenT.get(i).isErledigt()) {
				zwischenTDaten = new Object[]{zwischenT.get(i).getId(), zeitpunkt, zwischenT.get(i).getTerminName(), zwischenT.get(i).getKurzBeschreibung(), "erledigt"};
			}else {
				zwischenTDaten = new Object[]{zwischenT.get(i).getId(), zeitpunkt, zwischenT.get(i).getTerminName(), zwischenT.get(i).getKurzBeschreibung(), "offen"};
			}
			this.getModel().addRow(zwischenTDaten);
		}
		int row = Controller.getIDdesAnstehendenTermins();
		for (int i = 0; i < zwischenT.size(); i++) {
			if (row == zwischenT.get(i).getId()) {
				this.getTable().setColumnSelectionInterval(0, 0);
				this.getTable().setRowSelectionInterval(i,i);
			}
		}
	}
	
	//Getter und Setter
	public CardLayout getKarten() {
		return karten;
	}

	public JPanel getContentPane() {
		return contentPane;
	}
	
	public DefaultTableModel getModel() {
		return model;
	}

	public GUI getGUI() {
		return this;
	}

	public JTextField getTxt_terminName() {
		return txt_terminName;
	}

	public JTextField getTxt_terminBeschreibungKurz() {
		return txt_terminBeschreibungKurz;
	}

	public JTextArea getTxt_terminBeschreibungLang() {
		return txt_terminBeschreibungLang;
	}

	public DateTimePicker getDp_terminZeitpunkt() {
		return dp_terminZeitpunkt;
	}

	public DefaultTableModel getModel_terminTeilnehmer() {
		return model_terminTeilnehmer;
	}

	public DefaultTableModel getModel_terminErinnerung() {
		return model_terminErinnerung;
	}

	public JTable getTable_terminTeilnehmer() {
		return table_terminTeilnehmer;
	}

	public ButtonGroup getButtonGroupRepeat() {
		return buttonGroupRepeat;
	}

	public JCheckBox getChckbx_einmalig() {
		return chckbx_einmalig;
	}

	public JCheckBox getChckbx_taeglich() {
		return chckbx_taeglich;
	}

	public JCheckBox getChckbx_woechentlich() {
		return chckbx_woechentlich;
	}

	public JCheckBox getChckbx_monatlich() {
		return chckbx_monatlich;
	}

	public JCheckBox getChckbx_jaehrlich() {
		return chckbx_jaehrlich;
	}

	public JSpinner getNumberChooser() {
		return numberChooser;
	}

	public JTable getTable() {
		return table;
	}
	
}
