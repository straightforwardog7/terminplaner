package gui;

public class LoginStarter extends Thread{
	
	Login login;
	
	public void run() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		StartGUI.frame.setVisible(false);
		login = new Login();
	}

	public Login getLogin() {
		return this.login;
	}
}
