package gui;

import logic.Controller;
import model.Termin;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.Component;

class ToolTipRender extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	public Component getTableCellRendererComponent(
                        JTable table, Object value,
                        boolean isSelected, boolean hasFocus,
                        int row, int column) {
		JLabel c = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		Termin t = Controller.getTermin((int) StartGUI.frame.getLoginStarter().getLogin().getPlaner().getModel().getValueAt(row, 0));
		c.setToolTipText(t.getBeschreibung());
		return c;
	}
}
