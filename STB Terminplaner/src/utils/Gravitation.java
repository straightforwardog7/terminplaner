package utils;

import logic.ControllerEasterEgg;
import model.Tenbusch;
import guiEasterEgg.EasterEggGUI;

public class Gravitation extends Thread {

	@Override
	public void run() {
		while (!EasterEggGUI.gameScreen.isBeenden()) {
			while (!EasterEggGUI.gameScreen.isPause()) {
				ControllerEasterEgg.tenbusch.setGeschwindigkeit(
						ControllerEasterEgg.tenbusch.getGeschwindigkeit() - Tenbusch.getGravitation());
				ControllerEasterEgg.tenbusch.setPosY(
						(ControllerEasterEgg.tenbusch.getPosY() - ControllerEasterEgg.tenbusch.getGeschwindigkeit()));
				try {
					Thread.sleep(30);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}			
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
