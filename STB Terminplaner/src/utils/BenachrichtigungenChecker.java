package utils;

import logic.Controller;
import model.Termin;

import java.time.LocalDateTime;
import java.util.List;

public class BenachrichtigungenChecker extends Thread {

	public void run() {
		while (true) {
			List<Termin> terminListe = Controller.getTermine();
			int anzahl_Termine = terminListe.size();
			for (int i = 0; i < anzahl_Termine; i++) {
				Termin zwischenT = terminListe.get(i);
				if (zwischenT.getZeitpunkt().getYear() == (LocalDateTime.now().getYear()) && zwischenT.getZeitpunkt().getDayOfYear() == (LocalDateTime.now().getDayOfYear())
						&& zwischenT.getZeitpunkt().getHour() == (LocalDateTime.now().getHour()) && zwischenT.getZeitpunkt().getMinute() == (LocalDateTime.now().getMinute()) && !zwischenT.isBenachrichtigt()) {
					new Benachrichtigung("" + zwischenT.getTerminName() + " ist jetzt f\u00e4llig").zeigeBenachrichtigung();
					zwischenT.setBenachrichtigt(true);
					Controller.bearbeiteTermin(zwischenT);
				}
				for (int j = 0; j < zwischenT.getErinnerungsZeitpunkte().size(); j++) {
					LocalDateTime zwischen = zwischenT.getErinnerungsZeitpunkte().get(j).getErinnerungsZeitpunkt();
					if (zwischen.getYear() == (LocalDateTime.now().getYear()) && zwischen.getDayOfYear() == (LocalDateTime.now().getDayOfYear())
							&& zwischen.getHour() == (LocalDateTime.now().getHour()) && zwischen.getMinute() == (LocalDateTime.now().getMinute()) && !zwischenT.getErinnerungsZeitpunkte().get(j).isBenachrichtigt()) {
						new Benachrichtigung("" + zwischenT.getTerminName() + " ist bald f\u00e4llig").zeigeBenachrichtigung();
						zwischenT.getErinnerungsZeitpunkte().get(j).setBenachrichtigt(true);
						Controller.bearbeiteTermin(zwischenT);
					}
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
