package utils;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;

import data.Images;

public class Benachrichtigung {

	SystemTray tray;
	Image image;
	TrayIcon trayIcon;
	String message;
	
	public Benachrichtigung(String message) {
		this.setMessage(message);
		this.setTray(SystemTray.getSystemTray());
		this.setImage(Images.logo_STB);
		this.setTrayIcon(new TrayIcon(this.getImage(), "STB-Terminplaner"));
		this.getTrayIcon().setImageAutoSize(true);
		this.getTrayIcon().setToolTip("System tray icon demo");
		try {
			this.getTray().add(trayIcon);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}
	
	public void zeigeBenachrichtigung() {
		this.getTrayIcon().displayMessage(this.getMessage(), "Terminerinnerung", MessageType.INFO);
	}

	public SystemTray getTray() {
		return tray;
	}

	public void setTray(SystemTray tray) {
		this.tray = tray;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public TrayIcon getTrayIcon() {
		return trayIcon;
	}

	public void setTrayIcon(TrayIcon trayIcon) {
		this.trayIcon = trayIcon;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
