package utils;

public class Score {
	private static int score;
	private static int highScore = 0;

	public static int getHighScore() {
		return highScore;
	}

	public static void setHighScore(int highScore) {
		Score.highScore = highScore;
	}

	public static int getScore() {
		return score;
	}

	public static void setScore(int score) {
		Score.score = score;
	}
}
