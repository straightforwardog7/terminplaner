package utils;

import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import guiEasterEgg.EasterEggGUI;
import logic.ControllerEasterEgg;

public class Kollision extends Thread {

	@Override
	public void run() {
		while (!EasterEggGUI.gameScreen.isBeenden()) {
			int minPipe = 0;
			while (!EasterEggGUI.gameScreen.isPause()) {
				Ellipse2D tenbusch = new Ellipse2D.Double(ControllerEasterEgg.tenbusch.getPosX(),
						ControllerEasterEgg.tenbusch.getPosY(), ControllerEasterEgg.tenbusch.getWidth(),
						ControllerEasterEgg.tenbusch.getHeight());
				Rectangle2D pipe;

				// pr�ft, ob die Hitbox einer Pipe die von Tenbusch schneidet
				for (int i = minPipe; i < ControllerEasterEgg.pipes.size(); i++) {
					// ignoriert Pipes au�erhalb des Bildschirms
					pipe = new Rectangle(ControllerEasterEgg.pipes.get(i).getPosX(),
							ControllerEasterEgg.pipes.get(i).getPosY(),
							ControllerEasterEgg.pipes.get(i).getPipe().getWidth(null),
							ControllerEasterEgg.pipes.get(i).getPipe().getHeight(null));
					if (ControllerEasterEgg.pipes.get(i).getPosX() < 0) {
						minPipe++;
					}
					if (tenbusch.intersects(pipe)) {
						ControllerEasterEgg.gameOver();
						i = ControllerEasterEgg.pipes.size();
					}

				}
				// Wenn Tebusch den unteren Bildschirmrand ber�hrt erfolg ein Gameover
				if (tenbusch.getY() + tenbusch.getHeight() >= 900) {
					ControllerEasterEgg.gameOver();
				}
				// Pr�ft auf eine Kollision zwischen Tenbusch und Pipes au�erhalb des
				// Bildschirms im oberen Bereich
				if (tenbusch.getY() <= 0) {
					for (int j = 0; j < ControllerEasterEgg.pipes.size(); j++) {

						if (ControllerEasterEgg.pipes.get(j).getPosX() >= ControllerEasterEgg.tenbusch.getPosX()
								&& ControllerEasterEgg.pipes.get(j).getPosX() <= ControllerEasterEgg.tenbusch.getPosX()
										+ ControllerEasterEgg.tenbusch.getWidth()) {
							ControllerEasterEgg.gameOver();
							j = 0;
						}

					}
				}
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
