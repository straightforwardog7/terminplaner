package model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.List;

public class Termin {

	//Konstanten fuer einen sich wiederholenden Termin
	public static final int ALLE_TAGE = 0;
	public static final int ALLE_WOCHEN = 1;
	public static final int ALLE_MONATE = 2;
	public static final int ALLE_JAHRE = 3;

	private int id;
	private String terminName;
	private String kurzBeschreibung;
	private String beschreibung;
	private LocalDateTime zeitpunkt;
	private List<Benutzer> teilnehmer;
	private List<Erinnerung> erinnerungsZeitpunkte;
	private boolean erledigt;
	private boolean benachrichtigt = false;

	//Attribute fuer sich wiederholenden Termin, standardmaessig deaktiviert
	private int haeufigkeit = -1;
	private int artHaeufigkeit = -1;

	public Termin(int id, String terminName, String kurzBeschreibung, String beschreibung, LocalDateTime zeitpunkt,
				  List<Benutzer> teilnehmer, List<Erinnerung> erinnerungsZeitpunkte, boolean erledigt, boolean benachrichtigt) {
		this.setId(id);
		this.setTerminName(terminName);
		this.setKurzBeschreibung(kurzBeschreibung);
		this.setBeschreibung(beschreibung);
		this.setZeitpunkt(zeitpunkt);
		this.setTeilnehmer(teilnehmer);
		this.setErinnerungsZeitpunkte(erinnerungsZeitpunkte);
		this.setErledigt(erledigt);
		this.setBenachrichtigt(benachrichtigt);
	}

	public Termin(
			@JsonProperty("id") int id,
			@JsonProperty("terminName") String terminName,
			@JsonProperty("kurzBeschreibung") String kurzBeschreibung,
			@JsonProperty("beschreibung") String beschreibung,
			@JsonProperty("zeitpunkt") LocalDateTime zeitpunkt,
			@JsonProperty("teilnehmer") List<Benutzer> teilnehmer,
			@JsonProperty("erinnerungsZeitpunkte") List<Erinnerung> erinnerungsZeitpunkte,
			@JsonProperty("erledigt") boolean erledigt,
			@JsonProperty("haeufigkeit") int haeufigkeit, @JsonProperty("artHaeufigkeit") int artHaeufigkeit,
			@JsonProperty("benachrichtigt") boolean benachrichtigt) {

		this(id, terminName, kurzBeschreibung, beschreibung, zeitpunkt, teilnehmer, erinnerungsZeitpunkte, erledigt, benachrichtigt);
		this.haeufigkeit = haeufigkeit; //TODO Bessere Zuweisung noetig
		this.artHaeufigkeit = artHaeufigkeit;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTerminName() {
		return terminName;
	}

	public void setTerminName(String terminName) {
		this.terminName = terminName;
	}

	public String getKurzBeschreibung() {
		return kurzBeschreibung;
	}

	public void setKurzBeschreibung(String kurzBeschreibung) {
		this.kurzBeschreibung = kurzBeschreibung;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public LocalDateTime getZeitpunkt() {
		return zeitpunkt;
	}

	public void setZeitpunkt(LocalDateTime zeitpunkt) {
		this.zeitpunkt = zeitpunkt;
	}

	public List<Benutzer> getTeilnehmer() {
		return teilnehmer;
	}

	public void setTeilnehmer(List<Benutzer> teilnehmer) {
		this.teilnehmer = teilnehmer;
	}

	public List<Erinnerung> getErinnerungsZeitpunkte() {
		return erinnerungsZeitpunkte;
	}

	public void setErinnerungsZeitpunkte(List<Erinnerung> erinnerungsZeitpunkte) {
		this.erinnerungsZeitpunkte = erinnerungsZeitpunkte;
	}

	public boolean isErledigt() {
		return erledigt;
	}

	public void setErledigt(boolean erledigt) {
		this.erledigt = erledigt;
	}

	public int getHaeufigkeit() {
		return haeufigkeit;
	}

	public void setHaeufigkeit(int haeufigkeit) {
		this.haeufigkeit = Math.abs(haeufigkeit);
	}

	public int getArtHaeufigkeit() {
		return artHaeufigkeit;
	}

	public void setArtHaeufigkeit(int artHaeufigkeit) {
		if (artHaeufigkeit >= Termin.ALLE_TAGE && artHaeufigkeit <= Termin.ALLE_JAHRE) {
			this.artHaeufigkeit = haeufigkeit;
		}
	}

	public boolean isBenachrichtigt() {
		return benachrichtigt;
	}

	public void setBenachrichtigt(boolean benachrichtigt) {
		this.benachrichtigt = benachrichtigt;
	}

	@Override
	public String toString() {
		return "Termin{" +
				"id=" + id +
				", terminName='" + terminName + '\'' +
				", kurzBeschreibung='" + kurzBeschreibung + '\'' +
				", beschreibung='" + beschreibung + '\'' +
				", zeitpunkt=" + zeitpunkt +
				", teilnehmer=" + teilnehmer +
				", erinnerungsZeitpunkte=" + erinnerungsZeitpunkte +
				", erledigt=" + erledigt +
				", haeufigkeit=" + haeufigkeit +
				", artHaeufigkeit=" + artHaeufigkeit +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Termin termin = (Termin) o;

		if (getId() != termin.getId()) return false;
		if (isErledigt() != termin.isErledigt()) return false;
		if (getHaeufigkeit() != termin.getHaeufigkeit()) return false;
		if (getArtHaeufigkeit() != termin.getArtHaeufigkeit()) return false;
		if (!getTerminName().equals(termin.getTerminName())) return false;
		if (!getKurzBeschreibung().equals(termin.getKurzBeschreibung())) return false;
		if (!getBeschreibung().equals(termin.getBeschreibung())) return false;
		if (!getZeitpunkt().equals(termin.getZeitpunkt())) return false;
		if (!getTeilnehmer().equals(termin.getTeilnehmer())) return false;
		return getErinnerungsZeitpunkte().equals(termin.getErinnerungsZeitpunkte());
	}
}
