package model;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

public class Tenbusch {

	private int posX;
	private int posY;
	private Image tenbuschBild;
	private int geschwindigkeit;
	private static final int GRAVITATION = 1;

	public Tenbusch(int initialWidth, int initialHeight) {
		super();
		this.posX = 150;
		this.posY = 300;
		this.tenbuschBild = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/pictures/Tenbusch.png"));
		scaleTenbusch(initialWidth, initialHeight);
	}

	public int getWidth() {
		try {
			return tenbuschBild.getWidth(null);
		} catch (Exception e) {
			return -1;
		}
	}

	public int getHeight() {
		try {
			return tenbuschBild.getHeight(null);
		} catch (Exception e) {
			return -1;
		}
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getCenterPosX() {
		return posX + this.getWidth() / 2;
	}

	public int getCenterPosY() {
		return posY + this.getHeight() / 2;
	}

	public Image getTenbuschBild() {
		return tenbuschBild;
	}

	public void setTenbuschBild(Image tenbuschBild) {
		this.tenbuschBild = tenbuschBild;
	}

	public void scaleTenbusch(int width, int height) {
		tenbuschBild = tenbuschBild.getScaledInstance(width, height, Image.SCALE_SMOOTH);
	}

	public Rectangle getRectangle() {
		return (new Rectangle(posX, posY, tenbuschBild.getWidth(null), tenbuschBild.getHeight(null)));
	}

	public int getGeschwindigkeit() {
		return geschwindigkeit;
	}

	public void setGeschwindigkeit(int geschwindigkeit) {
		this.geschwindigkeit = geschwindigkeit;
	}

	public static int getGravitation() {
		return GRAVITATION;
	}

	public BufferedImage getBI() {
		BufferedImage bi = new BufferedImage(tenbuschBild.getWidth(null), tenbuschBild.getHeight(null),
				BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.getGraphics();
		g.drawImage(tenbuschBild, 0, 0, null);
		g.dispose();
		return bi;
	}
}
