package model;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

public class Pipe {

	int posX;
	int posY;
	Image pipe;
	boolean isTopPipe;

	public Pipe(int posX, int posY, boolean isTopPipe) {
		super();
		this.posY = posY;
		this.posX = posX;
		this.isTopPipe = isTopPipe;
		if (this.isTopPipe)
			this.pipe = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/pictures/TopPipe.png"));
		else
			this.pipe = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/pictures/Pipe.png"));

	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public Image getPipe() {
		return pipe;
	}

	public void setPipe(Image pipe) {
		this.pipe = pipe;
	}

	public boolean isTopPipe() {
		return isTopPipe;
	}

	public void setTopPipe(boolean isTopPipe) {
		this.isTopPipe = isTopPipe;
	}

	public Rectangle getRectangle() {
		Rectangle hitbox = new Rectangle(posX, posY, pipe.getWidth(null), pipe.getHeight(null));
		return hitbox;
	}

	public BufferedImage getBI() {
		BufferedImage bi = new BufferedImage(pipe.getWidth(null), pipe.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.getGraphics();
		g.drawImage(pipe, 0, 0, null);
		g.dispose();
		return bi;
	}
}
