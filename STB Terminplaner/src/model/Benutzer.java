package model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Benutzer {
	private String name;

	public Benutzer(
			@JsonProperty("name") String name) {
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Benutzer benutzer = (Benutzer) o;
		return getName().equals(benutzer.getName());
	}

	@Override
	public String toString() {
		return "Benutzer{" +
				"name='" + name + '\'' +
				'}';
	}
}
