package model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class Erinnerung {
	private LocalDateTime erinnerungsZeitpunkt;
	private boolean benachrichtigt = false;

	public Erinnerung(@JsonProperty("erinnerungsZeitpunkt") LocalDateTime erinnerungsZeitpunkt,
					  @JsonProperty("benachrichtigt") boolean benachrichtigt) {
		this.setErinnerungsZeitpunkt(erinnerungsZeitpunkt);
		this.setBenachrichtigt(benachrichtigt);
	}

	public LocalDateTime getErinnerungsZeitpunkt() {
		return erinnerungsZeitpunkt;
	}

	public void setErinnerungsZeitpunkt(LocalDateTime erinnerungsZeitpunkt) {
		this.erinnerungsZeitpunkt = erinnerungsZeitpunkt;
	}

	public boolean isBenachrichtigt() {
		return benachrichtigt;
	}

	public void setBenachrichtigt(boolean benachrichtigt) {
		this.benachrichtigt = benachrichtigt;
	}

	@Override
	public String toString() {
		return this.getErinnerungsZeitpunkt().toString();
	}
}
