package model;

import java.awt.Image;
import java.awt.Toolkit;

public class Hintergrund {

	private int posX;
	private Image hintergrundBild;

	public Hintergrund(int posX) {
		super();
		this.posX = posX;
		this.hintergrundBild = Toolkit.getDefaultToolkit()
				.getImage(this.getClass().getResource("/pictures/hintergrund.png"));
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public Image getHintergrundBild() {
		return hintergrundBild;
	}

	public void setHintergrundBild(Image hintergrundBild) {
		this.hintergrundBild = hintergrundBild;
	}

}
